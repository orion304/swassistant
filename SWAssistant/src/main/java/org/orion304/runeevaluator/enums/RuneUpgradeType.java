package main.java.org.orion304.runeevaluator.enums;

import java.util.HashMap;

public enum RuneUpgradeType {

	GRINDSTONE(2), ENCHANTMENT(1);

	private static HashMap<Integer, RuneUpgradeType> runeUpgradeTypesById = new HashMap<>();

	static {
		for (RuneUpgradeType runeUpgradeType : values()) {
			runeUpgradeTypesById.put(runeUpgradeType.id, runeUpgradeType);
		}
	}

	public static RuneUpgradeType getById(int id) {
		if (runeUpgradeTypesById.containsKey(id)) {
			return runeUpgradeTypesById.get(id);
		}

		return GRINDSTONE;
	}

	private int id;

	RuneUpgradeType(int id) {
		this.id = id;
	}

}
