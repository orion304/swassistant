package main.java.org.orion304.runeevaluator.enums;

public enum StatType {

	HP("HP%", false, true, 63), ATK("ATK%", true, false, 63), DEF("DEF%", false, true, 63), SPD("SPD", false, false,
			42), CR("Crit Rate", true, false,
					58), CD("Crit Dmg", true, false, 80), RES("RES", false, true, 64), ACC("ACC", false, false, 64);// ,
																													// WORTHLESS("Worthless",
																													// false,
																													// false,
																													// 100);

	public static final StatType[] slot2 = { HP, ATK, DEF, SPD };
	public static final StatType[] slot4 = { HP, ATK, DEF, CR, CD };
	public static final StatType[] slot6 = { HP, ATK, DEF, RES, ACC };

	private String displayString;
	public final boolean isAttackStat;
	public final boolean isDefenseStat;

	public final int maxStat;

	StatType(String displayString, boolean isAttackStat, boolean isDefenseStat, int maxStat) {
		this.displayString = displayString;
		this.isAttackStat = isAttackStat;
		this.isDefenseStat = isDefenseStat;
		this.maxStat = maxStat;
	}

	@Override
	public String toString() {
		return this.displayString;
	}

}
