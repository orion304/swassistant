package main.java.org.orion304.runeevaluator.enums;

import java.util.HashMap;

public enum RuneType {

	ACCURACY(22), ANY(0), BLADE(4, Dungeon.GIANTS), DESPAIR(10, 4, Dungeon.GIANTS), DESTROY(18,
			Dungeon.NECRO), DETERMINATION(20), ENDURE(7, Dungeon.DRAGONS), ENERGY(1, Dungeon.GIANTS), ENHANCE(
					21), FATAL(8, 4, Dungeon.GIANTS), FIGHT(19), FOCUS(6, Dungeon.DRAGONS), GUARD(2,
							Dungeon.DRAGONS), NEMESIS(14, Dungeon.NECRO), RAGE(5, 4, Dungeon.NECRO), REVENGE(17,
									Dungeon.DRAGONS), SHIELD(16, Dungeon.DRAGONS), SWIFT(3, 4,
											Dungeon.GIANTS), TOLERANCE(23), VAMPIRE(11, 4, Dungeon.NECRO), VIOLENT(13,
													4, Dungeon.DRAGONS), WILL(15, Dungeon.NECRO);

	private static HashMap<Integer, RuneType> runeTypesById = new HashMap<>();

	static {
		for (RuneType runeType : values()) {
			runeTypesById.put(runeType.id, runeType);
		}
	}

	public static RuneType getById(int id) {
		if (runeTypesById.containsKey(id)) {
			return runeTypesById.get(id);
		}

		return ANY;
	}

	private String displayString;

	private int id;
	public final Dungeon dropsFrom;
	public final int setSize;

	RuneType(int id) {
		this(id, null);
	}

	RuneType(int id, Dungeon dropsFrom) {
		this(id, 2, dropsFrom);
	}

	RuneType(int id, int setSize, Dungeon dropsFrom) {
		this.displayString = super.toString().substring(0, 1) + super.toString().substring(1).toLowerCase();
		this.id = id;
		this.setSize = setSize;
		this.dropsFrom = dropsFrom;
	}

	@Override
	public String toString() {
		return this.displayString;
	}

}
