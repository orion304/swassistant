package main.java.org.orion304.runeevaluator.enums;

public enum RuneWorth {

	HIGH("High", 1.0), MEDIUM("Medium", 0.7), LOW("Low", 0.3), NONE("None", 0.0);

	public final double multiplier;
	private final String displayString;

	RuneWorth(String displayString, double multiplier) {
		this.displayString = displayString;
		this.multiplier = multiplier;
	}

	@Override
	public String toString() {
		return this.displayString;
	}

}
