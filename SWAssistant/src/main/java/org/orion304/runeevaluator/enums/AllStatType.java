package main.java.org.orion304.runeevaluator.enums;

import java.util.HashMap;

public enum AllStatType {
	HP_flat(1, null), HP(2, StatType.HP), ATK_flat(3, null), ATK(4, StatType.ATK), DEF_flat(5, null), DEF(6,
			StatType.DEF), SPD(8, StatType.SPD), CRate(9,
					StatType.CR), CDmg(10, StatType.CD), RES(11, StatType.RES), ACC(12, StatType.ACC);

	private static HashMap<Integer, AllStatType> statsById = new HashMap<>();

	static {
		for (AllStatType stat : values()) {
			statsById.put(stat.id, stat);
		}
	}

	public static AllStatType getById(int id) {
		if (statsById.containsKey(id)) {
			return statsById.get(id);
		} else {
			return null;
		}
	}

	public final int id;
	public final StatType statType;

	AllStatType(int id, StatType statType) {
		this.id = id;
		this.statType = statType;
	}

}
