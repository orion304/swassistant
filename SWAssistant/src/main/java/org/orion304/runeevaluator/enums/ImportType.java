package main.java.org.orion304.runeevaluator.enums;

public enum ImportType {

	RUNES("runes.csv"), MONSTERS("monsters.csv"), SETTINGS("*.json"), SAVE_SETTINGS(""), LOG_FILE(""), EXPORT_RUNES(
			"runes.csv");

	public final String filename;

	ImportType(String filename) {
		this.filename = filename;
	}

}
