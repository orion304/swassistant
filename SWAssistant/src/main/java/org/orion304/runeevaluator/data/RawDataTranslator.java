package main.java.org.orion304.runeevaluator.data;

import main.java.org.orion304.runeevaluator.enums.StatType;

public class RawDataTranslator {

	public static StatType getStatType(int i) {
		switch (i) {
		case 0:
			return null;
		case 1:
			return null; // Flat HP
		case 2:
			return StatType.HP;
		case 3:
			return null; // Flat ATK
		case 4:
			return StatType.ATK;
		case 5:
			return null; // Flat DEF
		case 6:
			return StatType.DEF;
		case 7:
			return null;
		case 8:
			return StatType.SPD;
		case 9:
			return StatType.CR;
		case 10:
			return StatType.CD;
		case 11:
			return StatType.RES;
		case 12:
			return StatType.ACC;
		default:
			return null;
		}
	}

}
