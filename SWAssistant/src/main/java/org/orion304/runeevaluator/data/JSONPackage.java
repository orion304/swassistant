package main.java.org.orion304.runeevaluator.data;

public class JSONPackage {

	public String summonerName;
	public int energy;
	public int maxEnergy;
	public int wings;
	public int maxWings;
	public int mana;
	public int crystals;
	public int rifts;
	public int maxRifts;
	public int sp;
	public int maxSp;
	public int gloryPoints;
	public int guildPoints;
	public String[] outputStrings = { "", "", "", "", "", "", "" };

}
