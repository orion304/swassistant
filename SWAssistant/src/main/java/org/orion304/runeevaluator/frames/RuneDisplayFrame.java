package main.java.org.orion304.runeevaluator.frames;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;

import main.java.org.orion304.runeevaluator.enums.ImportType;
import main.java.org.orion304.runeevaluator.main.LiveSync;
import main.java.org.orion304.runeevaluator.main.Main;
import main.java.org.orion304.runeevaluator.main.Monster;
import main.java.org.orion304.runeevaluator.panels.WizardStatsPanel;

public class RuneDisplayFrame extends JFrame {

	/**
	 *
	 */
	private static final long serialVersionUID = -5856261470306546276L;
	private JPanel topPanel;

	/**
	 * Create the frame.
	 */
	public RuneDisplayFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1024, 403);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnImportexport = new JMenu("Import/Export");
		menuBar.add(mnImportexport);

		JMenuItem mntmRunes = new JMenuItem("Import Runes");
		mntmRunes.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (Monster.monsters.size() == 0) {
					JOptionPane.showMessageDialog(topPanel, "Monsters must be set up before runes can be evaluated.");
				} else {
					Main.displayImportFrame(ImportType.RUNES);
				}
			}
		});

		JMenuItem mntmMonsters = new JMenuItem("Import Monsters");
		mntmMonsters.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Main.displayImportFrame(ImportType.MONSTERS);
			}
		});

		JMenuItem mntmClearMonsters = new JMenuItem("Clear Monsters");
		mntmClearMonsters.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Monster.clearMonsters();
			}
		});
		mnImportexport.add(mntmClearMonsters);
		mnImportexport.add(mntmMonsters);
		mnImportexport.add(mntmRunes);

		JMenuItem mntmMonsterSettings = new JMenuItem("Load Settings");
		mntmMonsterSettings.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Main.displayImportFrame(ImportType.SETTINGS);
			}
		});

		JMenuItem mntmExportRunes = new JMenuItem("Export Runes");
		mntmExportRunes.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ImportFrame.exportRunes();
			}
		});
		mnImportexport.add(mntmExportRunes);

		JSeparator separator = new JSeparator();
		mnImportexport.add(separator);

		JMenuItem mntmSaveSettings = new JMenuItem("Save Settings");
		mntmSaveSettings.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Main.displayImportFrame(ImportType.SAVE_SETTINGS);
			}
		});
		mnImportexport.add(mntmSaveSettings);
		mnImportexport.add(mntmMonsterSettings);

		JMenu mnSettings = new JMenu("Settings");
		menuBar.add(mnSettings);

		JMenuItem mntmMonsterSettings_1 = new JMenuItem("Monster Settings");
		mntmMonsterSettings_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Main.displayFrame(MonsterSettingsFrame.class);
			}
		});
		mnSettings.add(mntmMonsterSettings_1);

		JMenuItem mntmSetLogFile = new JMenuItem("Set Log File Path");
		mntmSetLogFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Main.displayImportFrame(ImportType.LOG_FILE);
			}
		});
		mnSettings.add(mntmSetLogFile);

		JMenu mnUtilities = new JMenu("Utilities");
		menuBar.add(mnUtilities);

		JMenuItem mntmNewRuneUses = new JMenuItem("New Rune Uses");
		mntmNewRuneUses.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Main.displayFrame(NewRuneUsesFrame.class);
			}
		});
		mnUtilities.add(mntmNewRuneUses);

		JMenuItem mntmBestMonsterRunes = new JMenuItem("Best Monster Runes");
		mntmBestMonsterRunes.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Main.displayFrame(BestMonsterRunesFrame.class);
			}
		});
		mnUtilities.add(mntmBestMonsterRunes);

		JMenuItem mntmLockRunes = new JMenuItem("Lock Runes");
		mntmLockRunes.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Main.displayFrame(LockRunesFrame.class);
			}
		});
		mnUtilities.add(mntmLockRunes);

		JMenuItem mntmRandomDungeon = new JMenuItem("Random dungeon");
		mntmRandomDungeon.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Main.pickRandomDungeon();
			}
		});
		mnUtilities.add(mntmRandomDungeon);

		topPanel = new JPanel();
		setContentPane(topPanel);
		GridBagLayout gbl_topPanel = new GridBagLayout();
		gbl_topPanel.columnWidths = new int[] { 493, 0 };
		gbl_topPanel.rowHeights = new int[] { 22, 0 };
		gbl_topPanel.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_topPanel.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		topPanel.setLayout(gbl_topPanel);

		JPanel panel_1 = new JPanel();
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 0;
		topPanel.add(panel_1, gbc_panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[] { 0, 0, 0, 0 };
		gbl_panel_1.rowHeights = new int[] { 0, 0 };
		gbl_panel_1.columnWeights = new double[] { 1.0, 1.0, 1.0, Double.MIN_VALUE };
		gbl_panel_1.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		panel_1.setLayout(gbl_panel_1);

		JPanel panel_4 = new JPanel();
		GridBagConstraints gbc_panel_4 = new GridBagConstraints();
		gbc_panel_4.insets = new Insets(0, 0, 0, 5);
		gbc_panel_4.fill = GridBagConstraints.BOTH;
		gbc_panel_4.gridx = 0;
		gbc_panel_4.gridy = 0;
		panel_1.add(panel_4, gbc_panel_4);

		WizardStatsPanel panel_2 = new WizardStatsPanel();
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.gridwidth = 2;
		gbc_panel_2.insets = new Insets(0, 0, 0, 5);
		gbc_panel_2.fill = GridBagConstraints.BOTH;
		gbc_panel_2.gridx = 1;
		gbc_panel_2.gridy = 0;
		panel_1.add(panel_2, gbc_panel_2);
		new LiveSync(panel_2);
		if (Main.useOptimizerIntegration) {
			panel_2.updateOutput("Optimizer data found, integration enabled.");
		}
		
	}

}
