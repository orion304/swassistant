package main.java.org.orion304.runeevaluator.frames;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import main.java.org.orion304.runeevaluator.enums.RuneType;
import main.java.org.orion304.runeevaluator.enums.RuneWorth;
import main.java.org.orion304.runeevaluator.enums.StatType;
import main.java.org.orion304.runeevaluator.main.Main;
import main.java.org.orion304.runeevaluator.main.Monster;

public class EditMonsterFrame extends JFrame {

	/**
	 *
	 */
	private static final long serialVersionUID = 1934788256203323568L;

	private Monster monster;
	private JPanel contentPane;
	private JTextField textField;
	private JLabel lblSet;
	private JLabel lblSet_1;
	private JLabel lblSet_2;
	private JComboBox<RuneType> comboBox;
	private JComboBox<RuneType> comboBox_1;
	private JComboBox<RuneType> comboBox_2;
	private JLabel lblSlot;
	private JLabel lblSlot_1;
	private JLabel lblSlot_2;
	private HashMap<StatType, JCheckBox> slot2CheckBoxes = new HashMap<>();
	private HashMap<StatType, JCheckBox> slot4CheckBoxes = new HashMap<>();
	private HashMap<StatType, JCheckBox> slot6CheckBoxes = new HashMap<>();
	private JLabel lblStatWorths;
	private JLabel lblWorth;
	private HashMap<StatType, JComboBox<RuneWorth>> worthComboBoxes = new HashMap<>();
	private JPanel panel;
	private JPanel panel_1;
	private JPanel panel_2;
	private JPanel panel_3;
	private JPanel panel_4;
	private JButton btnCancel;
	private JButton btnOk;

	/**
	 * Create the frame.
	 */
	public EditMonsterFrame(Monster monster) {
		this.monster = monster;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 621, 557);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		contentPane.add(panel, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 0, 0, 0 };
		gbl_panel.rowHeights = new int[] { 0, 0 };
		gbl_panel.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gbl_panel.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		panel.setLayout(gbl_panel);

		JLabel lblMonsterName = new JLabel("Monster Name");
		GridBagConstraints gbc_lblMonsterName = new GridBagConstraints();
		gbc_lblMonsterName.insets = new Insets(0, 0, 0, 5);
		gbc_lblMonsterName.gridx = 0;
		gbc_lblMonsterName.gridy = 0;
		panel.add(lblMonsterName, gbc_lblMonsterName);

		textField = new JTextField(monster.getName());
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 0;
		panel.add(textField, gbc_textField);
		textField.setColumns(10);

		panel_1 = new JPanel();
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.insets = new Insets(0, 0, 5, 0);
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 1;
		contentPane.add(panel_1, gbc_panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[] { 0, 0, 0, 0 };
		gbl_panel_1.rowHeights = new int[] { 0, 0, 0 };
		gbl_panel_1.columnWeights = new double[] { 1.0, 1.0, 1.0, Double.MIN_VALUE };
		gbl_panel_1.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		panel_1.setLayout(gbl_panel_1);

		lblSet = new JLabel("Set 1");
		GridBagConstraints gbc_lblSet = new GridBagConstraints();
		gbc_lblSet.insets = new Insets(0, 0, 5, 5);
		gbc_lblSet.gridx = 0;
		gbc_lblSet.gridy = 0;
		panel_1.add(lblSet, gbc_lblSet);

		lblSet_1 = new JLabel("Set 2");
		GridBagConstraints gbc_lblSet_1 = new GridBagConstraints();
		gbc_lblSet_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblSet_1.gridx = 1;
		gbc_lblSet_1.gridy = 0;
		panel_1.add(lblSet_1, gbc_lblSet_1);

		lblSet_2 = new JLabel("Set 3");
		GridBagConstraints gbc_lblSet_2 = new GridBagConstraints();
		gbc_lblSet_2.insets = new Insets(0, 0, 5, 0);
		gbc_lblSet_2.gridx = 2;
		gbc_lblSet_2.gridy = 0;
		panel_1.add(lblSet_2, gbc_lblSet_2);

		comboBox = new JComboBox<>(RuneType.values());
		comboBox.insertItemAt(null, 0);
		comboBox.setSelectedItem(monster.getSet1());
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.insets = new Insets(0, 0, 0, 5);
		gbc_comboBox.gridx = 0;
		gbc_comboBox.gridy = 1;
		panel_1.add(comboBox, gbc_comboBox);

		comboBox_1 = new JComboBox<>(RuneType.values());
		comboBox_1.insertItemAt(null, 0);
		comboBox_1.setSelectedItem(monster.getSet2());
		GridBagConstraints gbc_comboBox_1 = new GridBagConstraints();
		gbc_comboBox_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_1.insets = new Insets(0, 0, 0, 5);
		gbc_comboBox_1.gridx = 1;
		gbc_comboBox_1.gridy = 1;
		panel_1.add(comboBox_1, gbc_comboBox_1);

		comboBox_2 = new JComboBox<>(RuneType.values());
		comboBox_2.insertItemAt(null, 0);
		comboBox_2.setSelectedItem(monster.getSet3());
		GridBagConstraints gbc_comboBox_2 = new GridBagConstraints();
		gbc_comboBox_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox_2.gridx = 2;
		gbc_comboBox_2.gridy = 1;
		panel_1.add(comboBox_2, gbc_comboBox_2);

		panel_2 = new JPanel();
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.insets = new Insets(0, 0, 5, 0);
		gbc_panel_2.fill = GridBagConstraints.BOTH;
		gbc_panel_2.gridx = 0;
		gbc_panel_2.gridy = 2;
		contentPane.add(panel_2, gbc_panel_2);
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
		gbl_panel_2.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
		gbl_panel_2.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
		gbl_panel_2.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
		panel_2.setLayout(gbl_panel_2);

		lblSlot = new JLabel("Slot 2");
		GridBagConstraints gbc_lblSlot = new GridBagConstraints();
		gbc_lblSlot.anchor = GridBagConstraints.WEST;
		gbc_lblSlot.insets = new Insets(0, 0, 5, 5);
		gbc_lblSlot.gridx = 1;
		gbc_lblSlot.gridy = 0;
		panel_2.add(lblSlot, gbc_lblSlot);

		lblSlot_1 = new JLabel("Slot 4");
		GridBagConstraints gbc_lblSlot_1 = new GridBagConstraints();
		gbc_lblSlot_1.anchor = GridBagConstraints.WEST;
		gbc_lblSlot_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblSlot_1.gridx = 2;
		gbc_lblSlot_1.gridy = 0;
		panel_2.add(lblSlot_1, gbc_lblSlot_1);

		lblSlot_2 = new JLabel("Slot 6");
		GridBagConstraints gbc_lblSlot_2 = new GridBagConstraints();
		gbc_lblSlot_2.anchor = GridBagConstraints.WEST;
		gbc_lblSlot_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblSlot_2.gridx = 3;
		gbc_lblSlot_2.gridy = 0;
		panel_2.add(lblSlot_2, gbc_lblSlot_2);

		JCheckBox chckbx;
		GridBagConstraints gbc_chckbx;
		int x = 1;
		int y = 1;
		for (StatType statType : StatType.slot2) {
			chckbx = new JCheckBox(statType.toString());
			chckbx.setSelected(monster.getSlot2().contains(statType));
			gbc_chckbx = new GridBagConstraints();
			gbc_chckbx.anchor = GridBagConstraints.WEST;
			gbc_chckbx.insets = new Insets(0, 0, 5, 5);
			gbc_chckbx.gridx = x;
			gbc_chckbx.gridy = y;
			panel_2.add(chckbx, gbc_chckbx);
			slot2CheckBoxes.put(statType, chckbx);
			y++;
		}

		x = 2;
		y = 1;
		for (StatType statType : StatType.slot4) {
			chckbx = new JCheckBox(statType.toString());
			chckbx.setSelected(monster.getSlot4().contains(statType));
			gbc_chckbx = new GridBagConstraints();
			gbc_chckbx.anchor = GridBagConstraints.WEST;
			gbc_chckbx.insets = new Insets(0, 0, 5, 5);
			gbc_chckbx.gridx = x;
			gbc_chckbx.gridy = y;
			panel_2.add(chckbx, gbc_chckbx);
			slot4CheckBoxes.put(statType, chckbx);
			y++;
		}

		x = 3;
		y = 1;
		for (StatType statType : StatType.slot6) {
			chckbx = new JCheckBox(statType.toString());
			chckbx.setSelected(monster.getSlot6().contains(statType));
			gbc_chckbx = new GridBagConstraints();
			gbc_chckbx.anchor = GridBagConstraints.WEST;
			gbc_chckbx.insets = new Insets(0, 0, 5, 5);
			gbc_chckbx.gridx = x;
			gbc_chckbx.gridy = y;
			panel_2.add(chckbx, gbc_chckbx);
			slot6CheckBoxes.put(statType, chckbx);
			y++;
		}

		panel_3 = new JPanel();
		GridBagConstraints gbc_panel_3 = new GridBagConstraints();
		gbc_panel_3.insets = new Insets(0, 0, 5, 0);
		gbc_panel_3.fill = GridBagConstraints.BOTH;
		gbc_panel_3.gridx = 0;
		gbc_panel_3.gridy = 3;
		contentPane.add(panel_3, gbc_panel_3);
		GridBagLayout gbl_panel_3 = new GridBagLayout();
		gbl_panel_3.columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
		gbl_panel_3.rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
		gbl_panel_3.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
		gbl_panel_3.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
		panel_3.setLayout(gbl_panel_3);

		lblStatWorths = new JLabel("Stat Worths:");
		GridBagConstraints gbc_lblStatWorths = new GridBagConstraints();
		gbc_lblStatWorths.gridwidth = 5;
		gbc_lblStatWorths.insets = new Insets(0, 0, 5, 5);
		gbc_lblStatWorths.gridx = 0;
		gbc_lblStatWorths.gridy = 0;
		panel_3.add(lblStatWorths, gbc_lblStatWorths);

		JComboBox<RuneWorth> runeWorthComboBox;
		GridBagConstraints gbc_runeWorthComboBox;
		x = 0;
		y = 1;
		for (StatType statType : StatType.values()) {
			lblWorth = new JLabel(statType.toString());
			GridBagConstraints gbc_lblHp = new GridBagConstraints();
			gbc_lblHp.anchor = GridBagConstraints.EAST;
			gbc_lblHp.insets = new Insets(0, 0, 5, 5);
			gbc_lblHp.gridx = x;
			gbc_lblHp.gridy = y;
			panel_3.add(lblWorth, gbc_lblHp);

			runeWorthComboBox = new JComboBox<>(RuneWorth.values());
			runeWorthComboBox.setSelectedItem(monster.getWorth(statType));
			gbc_runeWorthComboBox = new GridBagConstraints();
			gbc_runeWorthComboBox.fill = GridBagConstraints.HORIZONTAL;
			gbc_runeWorthComboBox.insets = new Insets(0, 0, 5, 5);
			gbc_runeWorthComboBox.gridx = x + 1;
			gbc_runeWorthComboBox.gridy = y;
			panel_3.add(runeWorthComboBox, gbc_runeWorthComboBox);
			worthComboBoxes.put(statType, runeWorthComboBox);

			y++;
			if (y > 4) {
				x = 2;
				y = 1;
			}
		}

		panel_4 = new JPanel();
		GridBagConstraints gbc_panel_4 = new GridBagConstraints();
		gbc_panel_4.fill = GridBagConstraints.BOTH;
		gbc_panel_4.gridx = 0;
		gbc_panel_4.gridy = 4;
		contentPane.add(panel_4, gbc_panel_4);
		GridBagLayout gbl_panel_4 = new GridBagLayout();
		gbl_panel_4.columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
		gbl_panel_4.rowHeights = new int[] { 0, 0 };
		gbl_panel_4.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
		gbl_panel_4.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		panel_4.setLayout(gbl_panel_4);

		btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				save();
			}
		});
		GridBagConstraints gbc_btnOk = new GridBagConstraints();
		gbc_btnOk.insets = new Insets(0, 0, 0, 5);
		gbc_btnOk.gridx = 1;
		gbc_btnOk.gridy = 0;
		panel_4.add(btnOk, gbc_btnOk);

		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		GridBagConstraints gbc_btnCancel = new GridBagConstraints();
		gbc_btnCancel.insets = new Insets(0, 0, 0, 5);
		gbc_btnCancel.gridx = 3;
		gbc_btnCancel.gridy = 0;
		panel_4.add(btnCancel, gbc_btnCancel);
	}

	private void close() {
		Main.displayFrame(MonsterSettingsFrame.class);
	}

	private void save() {
		monster.setName(textField.getText());
		monster.setSet1((RuneType) comboBox.getSelectedItem());
		monster.setSet2((RuneType) comboBox_1.getSelectedItem());
		monster.setSet3((RuneType) comboBox_2.getSelectedItem());

		monster.getSlot2().clear();
		for (StatType statType : slot2CheckBoxes.keySet()) {
			if (slot2CheckBoxes.get(statType).isSelected()) {
				monster.getSlot2().add(statType);
			}
		}

		monster.getSlot4().clear();
		for (StatType statType : slot4CheckBoxes.keySet()) {
			if (slot4CheckBoxes.get(statType).isSelected()) {
				monster.getSlot4().add(statType);
			}
		}

		monster.getSlot6().clear();
		for (StatType statType : slot6CheckBoxes.keySet()) {
			if (slot6CheckBoxes.get(statType).isSelected()) {
				monster.getSlot6().add(statType);
			}
		}

		for (StatType statType : worthComboBoxes.keySet()) {
			monster.setWorth(statType, (RuneWorth) worthComboBoxes.get(statType).getSelectedItem());
		}

		close();
	}

}
