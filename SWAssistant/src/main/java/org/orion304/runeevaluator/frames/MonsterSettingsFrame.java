package main.java.org.orion304.runeevaluator.frames;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import main.java.org.orion304.runeevaluator.enums.RuneWorth;
import main.java.org.orion304.runeevaluator.main.Main;
import main.java.org.orion304.runeevaluator.main.Monster;

public class MonsterSettingsFrame extends JFrame {

	/**
	 *
	 */
	private static final long serialVersionUID = 9026907857704964492L;

	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public MonsterSettingsFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1060, 642);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 1.0, 0.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		contentPane.add(panel, gbc_panel);
		panel.setLayout(new GridLayout(1, 0, 0, 0));

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.getVerticalScrollBar().setUnitIncrement(20);
		panel.add(scrollPane);

		JPanel panel_1 = new JPanel();
		scrollPane.setViewportView(panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_panel_1.rowHeights = new int[] { 0, 0, 0 };
		gbl_panel_1.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
		gbl_panel_1.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		panel_1.setLayout(gbl_panel_1);

		JLabel lblMonster = new JLabel("Monster");
		GridBagConstraints gbc_lblMonster = new GridBagConstraints();
		gbc_lblMonster.insets = new Insets(0, 0, 5, 5);
		gbc_lblMonster.gridx = 0;
		gbc_lblMonster.gridy = 0;
		panel_1.add(lblMonster, gbc_lblMonster);

		JLabel lblRuneSets = new JLabel("Rune Sets");
		GridBagConstraints gbc_lblRuneSets = new GridBagConstraints();
		gbc_lblRuneSets.insets = new Insets(0, 0, 5, 5);
		gbc_lblRuneSets.gridx = 1;
		gbc_lblRuneSets.gridy = 0;
		panel_1.add(lblRuneSets, gbc_lblRuneSets);

		JLabel lblSlotslotslot = new JLabel("Slot 2/Slot 4/Slot 6");
		GridBagConstraints gbc_lblSlotslotslot = new GridBagConstraints();
		gbc_lblSlotslotslot.insets = new Insets(0, 0, 5, 5);
		gbc_lblSlotslotslot.gridx = 2;
		gbc_lblSlotslotslot.gridy = 0;
		panel_1.add(lblSlotslotslot, gbc_lblSlotslotslot);

		JLabel lblGoodStats = new JLabel("Good Stats");
		GridBagConstraints gbc_lblGoodStats = new GridBagConstraints();
		gbc_lblGoodStats.insets = new Insets(0, 0, 5, 5);
		gbc_lblGoodStats.gridx = 3;
		gbc_lblGoodStats.gridy = 0;
		panel_1.add(lblGoodStats, gbc_lblGoodStats);

		JLabel lblOkStats = new JLabel("OK Stats");
		GridBagConstraints gbc_lblOkStats = new GridBagConstraints();
		gbc_lblOkStats.insets = new Insets(0, 0, 5, 5);
		gbc_lblOkStats.gridx = 4;
		gbc_lblOkStats.gridy = 0;
		panel_1.add(lblOkStats, gbc_lblOkStats);

		JLabel lblBadStats = new JLabel("Bad Stats");
		GridBagConstraints gbc_lblBadStats = new GridBagConstraints();
		gbc_lblBadStats.insets = new Insets(0, 0, 5, 5);
		gbc_lblBadStats.gridx = 5;
		gbc_lblBadStats.gridy = 0;
		panel_1.add(lblBadStats, gbc_lblBadStats);

		JLabel lblWorthlessStats = new JLabel("Worthless Stats");
		GridBagConstraints gbc_lblWorthlessStats = new GridBagConstraints();
		gbc_lblWorthlessStats.insets = new Insets(0, 0, 5, 0);
		gbc_lblWorthlessStats.gridx = 6;
		gbc_lblWorthlessStats.gridy = 0;
		panel_1.add(lblWorthlessStats, gbc_lblWorthlessStats);

		int y = 1;
		for (Monster monster : Monster.monsters) {
			JButton btnButton = new JButton(monster.getName());
			btnButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					editMonster(monster);
				}
			});
			GridBagConstraints gbc_btnButton = new GridBagConstraints();
			gbc_btnButton.fill = GridBagConstraints.HORIZONTAL;
			gbc_btnButton.insets = new Insets(0, 0, 0, 5);
			gbc_btnButton.gridx = 0;
			gbc_btnButton.gridy = y;
			panel_1.add(btnButton, gbc_btnButton);

			JLabel lblMonRuneSets = new JLabel(monster.getRuneSets());
			GridBagConstraints gbc_lblMonRuneSets = new GridBagConstraints();
			gbc_lblMonRuneSets.insets = new Insets(0, 0, 5, 5);
			gbc_lblMonRuneSets.gridx = 1;
			gbc_lblMonRuneSets.gridy = y;
			panel_1.add(lblMonRuneSets, gbc_lblMonRuneSets);

			JLabel lblMonSlotslotslot = new JLabel(monster.getMainStats());
			GridBagConstraints gbc_lblMonSlotslotslot = new GridBagConstraints();
			gbc_lblMonSlotslotslot.insets = new Insets(0, 0, 5, 5);
			gbc_lblMonSlotslotslot.gridx = 2;
			gbc_lblMonSlotslotslot.gridy = y;
			panel_1.add(lblMonSlotslotslot, gbc_lblMonSlotslotslot);

			int x = 3;
			for (RuneWorth runeWorth : RuneWorth.values()) {
				JLabel lblMonStats = new JLabel(monster.getStatsByWorth(runeWorth));
				GridBagConstraints gbc_lblMonStats = new GridBagConstraints();
				gbc_lblMonStats.insets = new Insets(0, 0, 5, 5);
				gbc_lblMonStats.gridx = x;
				gbc_lblMonStats.gridy = y;
				panel_1.add(lblMonStats, gbc_lblMonStats);
				x++;
			}

			y++;
		}

		JButton btnButton = new JButton("New Monster");
		GridBagConstraints gbc_btnButton = new GridBagConstraints();
		gbc_btnButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnButton.insets = new Insets(0, 0, 0, 5);
		gbc_btnButton.gridx = 0;
		gbc_btnButton.gridy = y;
		panel_1.add(btnButton, gbc_btnButton);

		JButton btnOk = new JButton("OK");
		btnOk.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Main.displayFrame(RuneDisplayFrame.class);
			}
		});
		GridBagConstraints gbc_btnOk = new GridBagConstraints();
		gbc_btnOk.gridx = 0;
		gbc_btnOk.gridy = 1;
		contentPane.add(btnOk, gbc_btnOk);
	}

	private void editMonster(Monster monster) {
		Main.displayEditMonsterFrame(monster);
	}

}
