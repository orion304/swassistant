package main.java.org.orion304.runeevaluator.frames;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import main.java.org.orion304.runeevaluator.enums.RuneType;
import main.java.org.orion304.runeevaluator.enums.StatType;
import main.java.org.orion304.runeevaluator.main.Cache;
import main.java.org.orion304.runeevaluator.main.Main;
import main.java.org.orion304.runeevaluator.main.Monster;
import main.java.org.orion304.runeevaluator.main.RuneStatSlot;

public class NewRuneUsesFrame extends JFrame {

	/**
	 *
	 */
	private static final long serialVersionUID = 587819137143048689L;
	private static final Integer[] slots = { 1, 2, 3, 4, 5, 6 };
	private JPanel contentPane;
	private JComboBox<Integer> slotComboBox;
	private JComboBox<RuneType> runeSetComboBox;
	private JComboBox<StatType> statComboBox;
	private JTextArea lblMonsters;

	/**
	 * Create the frame.
	 */
	public NewRuneUsesFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 1.0, 1.0, 1.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 1.0, 1.0, 0.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 5);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		contentPane.add(panel, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 0, 0 };
		gbl_panel.rowHeights = new int[] { 0, 0, 0 };
		gbl_panel.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel.rowWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
		panel.setLayout(gbl_panel);

		JLabel lblSlot = new JLabel("Slot");
		GridBagConstraints gbc_lblSlot = new GridBagConstraints();
		gbc_lblSlot.anchor = GridBagConstraints.SOUTH;
		gbc_lblSlot.insets = new Insets(0, 0, 5, 0);
		gbc_lblSlot.gridx = 0;
		gbc_lblSlot.gridy = 0;
		panel.add(lblSlot, gbc_lblSlot);

		slotComboBox = new JComboBox<>(slots);
		slotComboBox.insertItemAt(null, 0);
		slotComboBox.setSelectedIndex(0);
		slotComboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				displayMonsters();
			}
		});
		GridBagConstraints gbc_slotComboBox = new GridBagConstraints();
		gbc_slotComboBox.anchor = GridBagConstraints.NORTH;
		gbc_slotComboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_slotComboBox.gridx = 0;
		gbc_slotComboBox.gridy = 1;
		panel.add(slotComboBox, gbc_slotComboBox);

		JPanel panel_1 = new JPanel();
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.insets = new Insets(0, 0, 5, 5);
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 1;
		gbc_panel_1.gridy = 0;
		contentPane.add(panel_1, gbc_panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[] { 0, 0 };
		gbl_panel_1.rowHeights = new int[] { 0, 0, 0 };
		gbl_panel_1.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_1.rowWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
		panel_1.setLayout(gbl_panel_1);

		JLabel lblRuneSet = new JLabel("Rune Set");
		GridBagConstraints gbc_lblRuneSet = new GridBagConstraints();
		gbc_lblRuneSet.anchor = GridBagConstraints.SOUTH;
		gbc_lblRuneSet.insets = new Insets(0, 0, 5, 0);
		gbc_lblRuneSet.gridx = 0;
		gbc_lblRuneSet.gridy = 0;
		panel_1.add(lblRuneSet, gbc_lblRuneSet);

		runeSetComboBox = new JComboBox<>(RuneType.values());
		runeSetComboBox.insertItemAt(null, 0);
		runeSetComboBox.setSelectedIndex(0);
		runeSetComboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				displayMonsters();
			}
		});
		GridBagConstraints gbc_runeSetComboBox = new GridBagConstraints();
		gbc_runeSetComboBox.anchor = GridBagConstraints.NORTH;
		gbc_runeSetComboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_runeSetComboBox.gridx = 0;
		gbc_runeSetComboBox.gridy = 1;
		panel_1.add(runeSetComboBox, gbc_runeSetComboBox);

		JPanel panel_2 = new JPanel();
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.insets = new Insets(0, 0, 5, 0);
		gbc_panel_2.fill = GridBagConstraints.BOTH;
		gbc_panel_2.gridx = 2;
		gbc_panel_2.gridy = 0;
		contentPane.add(panel_2, gbc_panel_2);
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[] { 0, 0 };
		gbl_panel_2.rowHeights = new int[] { 0, 0, 0 };
		gbl_panel_2.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_2.rowWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
		panel_2.setLayout(gbl_panel_2);

		JLabel lblStat = new JLabel("Stat");
		GridBagConstraints gbc_lblStat = new GridBagConstraints();
		gbc_lblStat.anchor = GridBagConstraints.SOUTH;
		gbc_lblStat.insets = new Insets(0, 0, 5, 0);
		gbc_lblStat.gridx = 0;
		gbc_lblStat.gridy = 0;
		panel_2.add(lblStat, gbc_lblStat);

		statComboBox = new JComboBox<>(StatType.values());
		statComboBox.insertItemAt(null, 0);
		statComboBox.setSelectedIndex(0);
		statComboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				displayMonsters();
			}
		});
		GridBagConstraints gbc_StatComboBox = new GridBagConstraints();
		gbc_StatComboBox.anchor = GridBagConstraints.NORTH;
		gbc_StatComboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_StatComboBox.gridx = 0;
		gbc_StatComboBox.gridy = 1;
		panel_2.add(statComboBox, gbc_StatComboBox);

		JPanel panel_3 = new JPanel();
		GridBagConstraints gbc_panel_3 = new GridBagConstraints();
		gbc_panel_3.insets = new Insets(0, 0, 5, 0);
		gbc_panel_3.gridwidth = 3;
		gbc_panel_3.fill = GridBagConstraints.BOTH;
		gbc_panel_3.gridx = 0;
		gbc_panel_3.gridy = 1;
		contentPane.add(panel_3, gbc_panel_3);
		GridBagLayout gbl_panel_3 = new GridBagLayout();
		gbl_panel_3.columnWidths = new int[] { 0, 0 };
		gbl_panel_3.rowHeights = new int[] { 0, 0 };
		gbl_panel_3.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_3.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		panel_3.setLayout(gbl_panel_3);

		lblMonsters = new JTextArea("Monsters");
		lblMonsters.setLineWrap(true);
		lblMonsters.setEditable(false);
		lblMonsters.setBackground(UIManager.getColor("Button.background"));
		GridBagConstraints gbc_lblMonsters = new GridBagConstraints();
		gbc_lblMonsters.fill = GridBagConstraints.BOTH;
		gbc_lblMonsters.gridx = 0;
		gbc_lblMonsters.gridy = 0;
		panel_3.add(lblMonsters, gbc_lblMonsters);

		JPanel panel_4 = new JPanel();
		GridBagConstraints gbc_panel_4 = new GridBagConstraints();
		gbc_panel_4.insets = new Insets(0, 0, 0, 5);
		gbc_panel_4.fill = GridBagConstraints.BOTH;
		gbc_panel_4.gridx = 1;
		gbc_panel_4.gridy = 2;
		contentPane.add(panel_4, gbc_panel_4);
		GridBagLayout gbl_panel_4 = new GridBagLayout();
		gbl_panel_4.columnWidths = new int[] { 0, 0 };
		gbl_panel_4.rowHeights = new int[] { 0, 0 };
		gbl_panel_4.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_4.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		panel_4.setLayout(gbl_panel_4);

		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Main.displayFrame(RuneDisplayFrame.class);
			}
		});
		GridBagConstraints gbc_btnOk = new GridBagConstraints();
		gbc_btnOk.gridx = 0;
		gbc_btnOk.gridy = 0;
		panel_4.add(btnOk, gbc_btnOk);

		Cache.buildCache();

	}

	private void displayMonsters() {
		Integer slot = slotComboBox.getItemAt(slotComboBox.getSelectedIndex());

		if (slot == null) {
			return;
		}

		RuneType type = runeSetComboBox.getItemAt(runeSetComboBox.getSelectedIndex());

		if (type == null) {
			return;
		}

		StatType stat = statComboBox.getItemAt(statComboBox.getSelectedIndex());

		String string;
		if (slot % 2 == 0) {
			// Even slot
			if (stat == null) {
				return;
			}
			RuneStatSlot runeStatSlot = new RuneStatSlot(type, stat, slot);
			string = "";
			for (Monster monster : Cache.monstersByRuneStatSlot.get(runeStatSlot)) {
				string += monster.getName() + ", ";
			}

			if (string.length() > 0) {
				string = string.substring(0, string.length() - 2);
			}
		} else {
			// Odd slot
			string = "";
			for (Monster monster : Cache.monstersByRuneType.get(type)) {
				string += monster.getName() + ", ";
			}

			if (string.length() > 0) {
				string = string.substring(0, string.length() - 2);
			}

		}

		lblMonsters.setText(string);

	}

}
