package main.java.org.orion304.runeevaluator.frames;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.TreeMap;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;

import main.java.org.orion304.runeevaluator.enums.StatType;
import main.java.org.orion304.runeevaluator.main.Main;
import main.java.org.orion304.runeevaluator.main.Monster;
import main.java.org.orion304.runeevaluator.main.Rune;
import main.java.org.orion304.runeevaluator.main.RuneRank;

public class BestMonsterRunesFrame extends JFrame {

	/**
	 *
	 */
	private static boolean lastOnlyFromInventory = false;
	private static int lastRunes = 10;

	private static final int runesPerSlot = 5;
	private static final long serialVersionUID = 7246039641119380298L;
	private static final int[] xBySlotNumber = { -1, 1, 2, 2, 1, 0, 0 };
	private static final int[] yBySlotNumber = { -1, 1, 1, 2, 2, 2, 1 };
	private JPanel contentPane;
	private JComboBox<Monster> monsterComboBox;
	private ArrayList<JTextArea> runeTextBoxes = new ArrayList<>();
	private JTextField oddRunes;
	private JCheckBox chckbxOnlyFromInventory;

	/**
	 * Create the frame.
	 */
	public BestMonsterRunesFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 728, 458);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 1.0, 1.0, 1.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 1.0, 1.0, 0.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		JPanel panel_7 = new JPanel();
		GridBagConstraints gbc_panel_7 = new GridBagConstraints();
		gbc_panel_7.gridwidth = 3;
		gbc_panel_7.insets = new Insets(0, 0, 5, 5);
		gbc_panel_7.fill = GridBagConstraints.BOTH;
		gbc_panel_7.gridx = 0;
		gbc_panel_7.gridy = 0;
		contentPane.add(panel_7, gbc_panel_7);
		GridBagLayout gbl_panel_7 = new GridBagLayout();
		gbl_panel_7.columnWidths = new int[] { 0, 0 };
		gbl_panel_7.rowHeights = new int[] { 0, 0 };
		gbl_panel_7.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_7.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		panel_7.setLayout(gbl_panel_7);

		monsterComboBox = new JComboBox<>();
		monsterComboBox.addItem(null);
		for (Monster monster : Monster.monsters) {
			monsterComboBox.addItem(monster);
		}
		monsterComboBox.setSelectedIndex(0);
		monsterComboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				showRunes(false);
			}
		});
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.gridx = 0;
		gbc_comboBox.gridy = 0;
		panel_7.add(monsterComboBox, gbc_comboBox);

		for (int slot = 1; slot <= 6; slot++) {
			JPanel panel = new JPanel();
			panel.setBorder(new BevelBorder(BevelBorder.LOWERED, Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK));
			GridBagConstraints gbc_panel = new GridBagConstraints();
			gbc_panel.insets = new Insets(0, 0, 5, 5);
			gbc_panel.fill = GridBagConstraints.BOTH;
			gbc_panel.gridx = xBySlotNumber[slot];
			gbc_panel.gridy = yBySlotNumber[slot];
			contentPane.add(panel, gbc_panel);
			GridBagLayout gbl_panel = new GridBagLayout();
			gbl_panel.columnWidths = new int[] { 0, 0 };
			gbl_panel.rowHeights = new int[] { 0, 0, 0, 0 };
			gbl_panel.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
			gbl_panel.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
			panel.setLayout(gbl_panel);

			for (int i = 0; i < runesPerSlot; i++) {
				JPanel runePanel = new JPanel();
				GridBagConstraints gbc_runePanel = new GridBagConstraints();
				gbc_runePanel.insets = new Insets(0, 0, 5, 0);
				gbc_runePanel.fill = GridBagConstraints.BOTH;
				gbc_runePanel.gridx = 0;
				gbc_runePanel.gridy = i;
				panel.add(runePanel, gbc_runePanel);
				GridBagLayout gbl_runePanel = new GridBagLayout();
				gbl_runePanel.columnWidths = new int[] { 0, 0 };
				gbl_runePanel.rowHeights = new int[] { 0, 0 };
				gbl_runePanel.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
				gbl_runePanel.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
				runePanel.setLayout(gbl_runePanel);

				JTextArea textArea = new JTextArea();
				textArea.setBackground(UIManager.getColor("Button.background"));
				textArea.setEditable(false);
				textArea.setLineWrap(true);
				GridBagConstraints gbc_textArea = new GridBagConstraints();
				gbc_textArea.fill = GridBagConstraints.BOTH;
				gbc_textArea.gridx = 0;
				gbc_textArea.gridy = 0;
				runePanel.add(textArea, gbc_textArea);

				runeTextBoxes.add(textArea);
			}
		}

		JPanel panel_6 = new JPanel();
		GridBagConstraints gbc_panel_6 = new GridBagConstraints();
		gbc_panel_6.gridwidth = 3;
		gbc_panel_6.fill = GridBagConstraints.BOTH;
		gbc_panel_6.gridx = 0;
		gbc_panel_6.gridy = 3;
		contentPane.add(panel_6, gbc_panel_6);
		GridBagLayout gbl_panel_6 = new GridBagLayout();
		gbl_panel_6.columnWidths = new int[] { 0, 0, 0 };
		gbl_panel_6.rowHeights = new int[] { 0, 0 };
		gbl_panel_6.columnWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
		gbl_panel_6.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		panel_6.setLayout(gbl_panel_6);

		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		GridBagConstraints gbc_btnOk = new GridBagConstraints();
		gbc_btnOk.insets = new Insets(0, 0, 0, 5);
		gbc_btnOk.gridx = 0;
		gbc_btnOk.gridy = 0;
		panel_6.add(btnOk, gbc_btnOk);

		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 1;
		gbc_panel.gridy = 0;
		panel_6.add(panel, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
		gbl_panel.rowHeights = new int[] { 0, 0 };
		gbl_panel.columnWeights = new double[] { 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_panel.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		panel.setLayout(gbl_panel);

		JLabel lblOfOdd = new JLabel("# of runes");
		GridBagConstraints gbc_lblOfOdd = new GridBagConstraints();
		gbc_lblOfOdd.anchor = GridBagConstraints.EAST;
		gbc_lblOfOdd.insets = new Insets(0, 0, 0, 5);
		gbc_lblOfOdd.gridx = 0;
		gbc_lblOfOdd.gridy = 0;
		panel.add(lblOfOdd, gbc_lblOfOdd);

		oddRunes = new JTextField();
		oddRunes.setText(Integer.toString(lastRunes));
		GridBagConstraints gbc_oddRunes = new GridBagConstraints();
		gbc_oddRunes.insets = new Insets(0, 0, 0, 5);
		gbc_oddRunes.fill = GridBagConstraints.HORIZONTAL;
		gbc_oddRunes.gridx = 1;
		gbc_oddRunes.gridy = 0;
		panel.add(oddRunes, gbc_oddRunes);
		oddRunes.setColumns(10);

		JButton btnExport = new JButton("Export");
		btnExport.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				showRunes(true);
			}
		});

		chckbxOnlyFromInventory = new JCheckBox("Only from inventory");
		chckbxOnlyFromInventory.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				showRunes(false);
			}
		});
		chckbxOnlyFromInventory.setSelected(lastOnlyFromInventory);
		GridBagConstraints gbc_chckbxOnlyFromInventory = new GridBagConstraints();
		gbc_chckbxOnlyFromInventory.insets = new Insets(0, 0, 0, 5);
		gbc_chckbxOnlyFromInventory.gridx = 3;
		gbc_chckbxOnlyFromInventory.gridy = 0;
		panel.add(chckbxOnlyFromInventory, gbc_chckbxOnlyFromInventory);
		GridBagConstraints gbc_btnExport = new GridBagConstraints();
		gbc_btnExport.gridx = 5;
		gbc_btnExport.gridy = 0;
		panel.add(btnExport, gbc_btnExport);
	}

	private void close() {
		try {
			lastRunes = Integer.parseInt(oddRunes.getText());
		} catch (Exception e) {

		}

		lastOnlyFromInventory = chckbxOnlyFromInventory.isSelected();

		Main.displayFrame(RuneDisplayFrame.class);
	}

	private String formatRune(Rune rune) {
		String string = "";
		string += "(" + rune.runeId + ") ";
		string += rune.stars + "* ";
		string += rune.runeType + " ";
		if (rune.slot % 2 == 0) {
			string += rune.statTypes.get(0) + " ";
		}

		StatType statType;
		int power;
		for (int i = 1; i <= 5; i++) {
			statType = rune.statTypes.get(i);
			power = rune.statValues.get(i);

			if (statType != null) {
				if (i == 1) {
					string += "[";
				}

				string += power + " " + statType;

				if (i == 1) {
					string += "]";
				}

				string += ", ";
			}
		}

		if (string.endsWith(", ")) {
			string = string.substring(0, string.length() - 2);
		}

		return string;
	}

	private void showRunes(boolean export) {
		Monster monster = (Monster) monsterComboBox.getSelectedItem();
		if (monster == null) {
			return;
		}

		if (Main.useOptimizerIntegration) {
			ImportFrame.importRunesFileFromOptimizer();
		}

		boolean onlyInventory = chckbxOnlyFromInventory.isSelected();

		int numOfRunes;
		try {
			numOfRunes = Integer.parseInt(this.oddRunes.getText());
		} catch (Exception e) {
			numOfRunes = 0;
		}

		String exportString = "";
		TreeMap<RuneRank, Rune> runes;
		for (int slot = 1; slot <= 6; slot++) {
			runes = monster.getSortedRunesBySlot(slot);

			int j = 0;
			for (RuneRank runeRank : runes.keySet()) {
				int index = (slot - 1) * runesPerSlot + j;
				Rune rune = runes.get(runeRank);

				if (rune.locked || (onlyInventory && !rune.inInventory
						&& !rune.equippedTo.equalsIgnoreCase(monster.getName()))) {
					continue;
				}

				if (!rune.speedFixed) {
					// continue;
				}

				if (export) {
					if (j < numOfRunes) {
						exportString += rune.runeId + ",";
					} else {
						break;
					}
				} else {
					if (j < runesPerSlot) {
						runeTextBoxes.get(index).setText(formatRune(rune));
					} else {
						break;
					}
				}

				j++;
			}

		}

		if (export) {
			if (exportString.endsWith(",")) {
				exportString = exportString.substring(0, exportString.length() - 2);
			}

			StringSelection selection = new StringSelection(exportString);
			Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			clipboard.setContents(selection, selection);
			JOptionPane.showInputDialog(this, "Exported Rune IDs", "Export", JOptionPane.PLAIN_MESSAGE, null, null,
					exportString);

		}

	}

}
