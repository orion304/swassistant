package main.java.org.orion304.runeevaluator.frames;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import main.java.org.orion304.runeevaluator.main.Main;
import main.java.org.orion304.runeevaluator.main.Rune;

public class LockRunesFrame extends JFrame {

	/**
	 *
	 */
	private static final long serialVersionUID = -3577241756816664438L;
	private JPanel contentPane;
	private JTextField textField;
	private JList<Integer> list;

	/**
	 * Create the frame.
	 */
	public LockRunesFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 1.0, 0.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 5);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		contentPane.add(panel, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 0, 0 };
		gbl_panel.rowHeights = new int[] { 0, 0 };
		gbl_panel.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		panel.setLayout(gbl_panel);

		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 0;
		panel.add(scrollPane, gbc_scrollPane);

		list = new JList<>();
		scrollPane.setViewportView(list);

		JPanel panel_2 = new JPanel();
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.insets = new Insets(0, 0, 5, 0);
		gbc_panel_2.fill = GridBagConstraints.BOTH;
		gbc_panel_2.gridx = 1;
		gbc_panel_2.gridy = 0;
		contentPane.add(panel_2, gbc_panel_2);
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[] { 0, 0 };
		gbl_panel_2.rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
		gbl_panel_2.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_2.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		panel_2.setLayout(gbl_panel_2);

		JLabel lblRuneIds = new JLabel("Rune IDs");
		GridBagConstraints gbc_lblRuneIds = new GridBagConstraints();
		gbc_lblRuneIds.insets = new Insets(0, 0, 5, 0);
		gbc_lblRuneIds.gridx = 0;
		gbc_lblRuneIds.gridy = 0;
		panel_2.add(lblRuneIds, gbc_lblRuneIds);

		textField = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.insets = new Insets(0, 0, 5, 0);
		gbc_textField.gridx = 0;
		gbc_textField.gridy = 1;
		panel_2.add(textField, gbc_textField);
		textField.setColumns(10);

		JButton btnLock = new JButton("Lock");
		btnLock.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				lockRunes();
			}
		});
		GridBagConstraints gbc_btnLock = new GridBagConstraints();
		gbc_btnLock.insets = new Insets(0, 0, 5, 0);
		gbc_btnLock.gridx = 0;
		gbc_btnLock.gridy = 2;
		panel_2.add(btnLock, gbc_btnLock);

		JButton btnUnlockSelected = new JButton("Unlock Selected");
		btnUnlockSelected.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				unlockSelected();
			}
		});
		GridBagConstraints gbc_btnUnlockSelected = new GridBagConstraints();
		gbc_btnUnlockSelected.gridx = 0;
		gbc_btnUnlockSelected.gridy = 4;
		panel_2.add(btnUnlockSelected, gbc_btnUnlockSelected);

		JPanel panel_1 = new JPanel();
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.gridwidth = 2;
		gbc_panel_1.insets = new Insets(0, 0, 0, 5);
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 1;
		contentPane.add(panel_1, gbc_panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[] { 0, 0 };
		gbl_panel_1.rowHeights = new int[] { 0, 0 };
		gbl_panel_1.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_1.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		panel_1.setLayout(gbl_panel_1);

		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Main.displayFrame(RuneDisplayFrame.class);
			}
		});
		GridBagConstraints gbc_btnOk = new GridBagConstraints();
		gbc_btnOk.gridx = 0;
		gbc_btnOk.gridy = 0;
		panel_1.add(btnOk, gbc_btnOk);

		refreshDisplay();
	}

	private void lockRunes() {
		String runeIdsTxt = textField.getText();
		String[] runes = runeIdsTxt.split(",");

		int runeId;
		ArrayList<Integer> runeIds = new ArrayList<>();
		for (String rune : runes) {
			rune = rune.trim();
			try {
				runeId = Integer.parseInt(rune);
			} catch (Exception e) {
				continue;
			}
			runeIds.add(runeId);

		}

		for (Rune rune : Rune.runes) {
			if (runeIds.contains(rune.runeId)) {
				rune.locked = true;
			}
		}

		textField.setText("");
		refreshDisplay();
	}

	private void refreshDisplay() {
		TreeSet<Integer> runeIds = new TreeSet<>();

		for (Rune rune : Rune.runes) {
			if (rune.locked) {
				runeIds.add(rune.runeId);
			}
		}

		list.setListData(runeIds.toArray(new Integer[runeIds.size()]));

	}

	private void unlockSelected() {

		List<Integer> selection = list.getSelectedValuesList();

		for (Rune rune : Rune.runes) {
			if (selection.contains(rune.runeId)) {
				rune.locked = false;
			}
		}

		refreshDisplay();
	}

}
