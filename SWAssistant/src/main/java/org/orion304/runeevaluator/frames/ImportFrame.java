package main.java.org.orion304.runeevaluator.frames;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.json.JSONArray;
import org.json.JSONObject;

import main.java.org.orion304.runeevaluator.enums.ImportType;
import main.java.org.orion304.runeevaluator.main.Cache;
import main.java.org.orion304.runeevaluator.main.Main;
import main.java.org.orion304.runeevaluator.main.Monster;
import main.java.org.orion304.runeevaluator.main.Rune;
import main.java.org.orion304.runeevaluator.main.SWAssistantProperties;
import main.java.org.orion304.runeevaluator.main.Settings;

public class ImportFrame extends JFrame {

	/**
	 *
	 */
	private static final long serialVersionUID = 5706463512213832930L;

	public static void exportRunes() {
		Rune.reevaluateRunes();
		Rune.outputEvaluatedRunes("manual_evaluatedRunes.txt");

		StringSelection selection = new StringSelection(Rune.getRuneTable());
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(selection, selection);
	}

	public static void importRunesFile(String filePath) {
		File file = new File(filePath);
		if (!file.exists()) {
			return;
		}

		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));

			String line;
			String[] splitString;

			Main.clearRunes();
			Cache.buildCache();

			boolean allArePoweredUp = true;

			for (int i = 0; true; i++) {
				line = br.readLine();
				if (line == null) {
					break;
				}

				if (!line.contains(",")) {
					continue;
				}

				if (i > 0) {
					splitString = line.split(",");
					if (splitString.length == 0 || splitString[0] == "") {
						break;
					}
					Rune r = new Rune(splitString);
					if (r.powerups > 0) {
						allArePoweredUp = false;
					}
				}

			}

			if (allArePoweredUp) {
				System.out.println("All runes are powered up!");
			}

			Main.filePaths.setRunesFilePath(filePath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void importRunesFileFromOptimizer() {
		File file = new File(Main.optimizerJsonFilePath);
		if (!file.exists()) {
			return;
		}

		BufferedReader br = null;
		String text = "";
		try {
			br = new BufferedReader(new FileReader(file));

			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			text = sb.toString();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		Main.clearRunes();
		Cache.buildCache();

		JSONObject json = new JSONObject(text);
		JSONArray runes = json.getJSONArray("runes");
		for (Object rune : runes) {
			if (rune instanceof JSONObject) {
				new Rune((JSONObject) rune);
			}
		}
	}

	private JPanel contentPane;
	private JTextField textField;

	private JFileChooser fileChooser;

	private final ImportType importType;

	/**
	 * Create the frame.
	 */
	public ImportFrame(ImportType importType) {
		this.importType = importType;

		String labelText = "Import from file (" + importType.filename + ")";
		if (importType == ImportType.SAVE_SETTINGS || importType == ImportType.EXPORT_RUNES) {
			labelText = "Save to file";
		} else if (importType == ImportType.LOG_FILE) {
			labelText = "Choose log file path";
		}

		fileChooser = new JFileChooser();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 135);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 0, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 1.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		JLabel lblFilePathrunescsv = new JLabel(labelText);
		GridBagConstraints gbc_lblFilePathrunescsv = new GridBagConstraints();
		gbc_lblFilePathrunescsv.gridwidth = 2;
		gbc_lblFilePathrunescsv.insets = new Insets(0, 0, 5, 5);
		gbc_lblFilePathrunescsv.gridx = 0;
		gbc_lblFilePathrunescsv.gridy = 0;
		contentPane.add(lblFilePathrunescsv, gbc_lblFilePathrunescsv);

		textField = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.gridwidth = 2;
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 0;
		gbc_textField.gridy = 1;
		contentPane.add(textField, gbc_textField);
		textField.setColumns(10);

		JButton button = new JButton("...");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int returnVal = fileChooser.showOpenDialog(contentPane);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();
					textField.setText(file.getAbsolutePath());
				}
			}
		});
		GridBagConstraints gbc_button = new GridBagConstraints();
		gbc_button.insets = new Insets(0, 0, 5, 0);
		gbc_button.gridx = 2;
		gbc_button.gridy = 1;
		contentPane.add(button, gbc_button);

		String btnImportText = "Import";
		if (importType == ImportType.SAVE_SETTINGS) {
			btnImportText = "Save";
		} else if (importType == ImportType.LOG_FILE) {
			btnImportText = "OK";
		}

		JButton btnImport = new JButton(btnImportText);
		btnImport.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				importFile(textField.getText());
			}
		});
		GridBagConstraints gbc_btnImport = new GridBagConstraints();
		gbc_btnImport.insets = new Insets(0, 0, 0, 5);
		gbc_btnImport.gridx = 0;
		gbc_btnImport.gridy = 2;
		contentPane.add(btnImport, gbc_btnImport);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		GridBagConstraints gbc_btnCancel = new GridBagConstraints();
		gbc_btnCancel.insets = new Insets(0, 0, 0, 5);
		gbc_btnCancel.gridx = 1;
		gbc_btnCancel.gridy = 2;
		contentPane.add(btnCancel, gbc_btnCancel);

		String previousFilePath = "";
		switch (importType) {
		case MONSTERS:
			previousFilePath = Main.filePaths.getMonstersFilePath();
			break;
		case RUNES:
			previousFilePath = Main.filePaths.getRunesFilePath();
			break;
		case EXPORT_RUNES:
			break;
		case SAVE_SETTINGS:
		case SETTINGS:
			previousFilePath = Main.filePaths.getSettingsFilePath();
			break;
		case LOG_FILE:
			previousFilePath = Main.filePaths.getLogFilePath();
			break;
		default:
			break;

		}
		textField.setText(previousFilePath);
	}

	private void close() {
		Main.displayFrame(RuneDisplayFrame.class);
	}

	private void importFile(String filePath) {
		switch (importType) {
		case MONSTERS:
			importMonstersFile(filePath);
			break;
		case RUNES:
			importRunesFile(filePath);
			close();
			break;
		case SETTINGS:
			importSettingsFile(filePath);
			break;
		case SAVE_SETTINGS:
			saveSettingsFile(filePath);
			break;
		case LOG_FILE:
			saveLogFilePath(filePath);
			break;
		default:
			break;
		}
	}

	private void importMonstersFile(String filePath) {
		File file = new File(filePath);
		if (!file.exists()) {
			return;
		}

		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));

			String line;
			String[] splitString;

			// Monster.clearMonsters();

			for (int i = 0; true; i++) {
				line = br.readLine();
				if (line == null) {
					break;
				}

				if (!line.contains(",")) {
					continue;
				}

				if (i > 0) {
					splitString = line.split(",");
					if (splitString.length == 0 || splitString[0] == "") {
						break;
					}
					if (!Monster.doesMonsterExist(splitString[1])) {
						new Monster(splitString);
					}
				}

			}

			Main.filePaths.setMonstersFilePath(filePath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		close();
	}

	private void importSettingsFile(String filePath) {
		Settings.readSettings(filePath);
		SWAssistantProperties.setImportPath(filePath);
		SWAssistantProperties.saveProperties();
		Main.filePaths.setSettingsFilePath(filePath);

		close();
	}

	private void saveLogFilePath(String filePath) {
		Main.filePaths.setLogFilePath(filePath);

		close();
	}

	private void saveSettingsFile(String filePath) {
		Main.filePaths.setSettingsFilePath(filePath);
		SWAssistantProperties.setImportPath(filePath);
		SWAssistantProperties.saveProperties();
		Settings.saveSettings(filePath);

		close();
	}

}
