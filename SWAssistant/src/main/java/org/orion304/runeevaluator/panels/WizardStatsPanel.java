package main.java.org.orion304.runeevaluator.panels;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.NumberFormat;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.json.JSONArray;
import org.json.JSONObject;

import main.java.org.orion304.runeevaluator.main.Packet;
import main.java.org.orion304.runeevaluator.main.Rune;

public class WizardStatsPanel extends JPanel {

	/**
	 *
	 */
	private static final long serialVersionUID = 2662695524616370418L;

	private static String[] outputStrings = { "", "", "", "", "", "", "" };
	private static int outputStringsLength = outputStrings.length;

	private JLabel lblWizardname;
	private JLabel lblWizardlevel;
	private JLabel lblEnergyamount;
	private JLabel lblEnergytime;
	private JLabel lblRiftcrystalamount;
	private JLabel lblWingsamount;
	private JLabel lblWingstime;
	private JLabel lblSocialpointamount;
	private JLabel lblManaamount;
	private JLabel lblHonorpointsamount;
	private JLabel lblCrystalsamount;
	private JLabel lblGuildpointamounts;
	private JPanel panel_1;
	private JTextArea textArea;

	/**
	 * Create the panel.
	 */
	public WizardStatsPanel() {
		GridBagLayout gridBagLayout_1 = new GridBagLayout();
		gridBagLayout_1.columnWidths = new int[] { 421, 0 };
		gridBagLayout_1.rowHeights = new int[] { 120, 0 };
		gridBagLayout_1.columnWeights = new double[] { 1.0, 1.0 };
		gridBagLayout_1.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		setLayout(gridBagLayout_1);

		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.insets = new Insets(0, 0, 0, 5);
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		add(panel, gbc_panel);

		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, 1.0, 1.0, 0.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 1.0, 0.0, 1.0, 1.0, 1.0, 1.0 };
		panel.setLayout(gridBagLayout);

		lblWizardname = new JLabel("WizardName");
		GridBagConstraints gbc_lblWizardname = new GridBagConstraints();
		gbc_lblWizardname.anchor = GridBagConstraints.EAST;
		gbc_lblWizardname.gridwidth = 4;
		gbc_lblWizardname.insets = new Insets(0, 0, 5, 5);
		gbc_lblWizardname.gridx = 0;
		gbc_lblWizardname.gridy = 0;
		panel.add(lblWizardname, gbc_lblWizardname);

		lblWizardlevel = new JLabel("WizardLevel");
		GridBagConstraints gbc_lblWizardlevel = new GridBagConstraints();
		gbc_lblWizardlevel.anchor = GridBagConstraints.WEST;
		gbc_lblWizardlevel.gridwidth = 3;
		gbc_lblWizardlevel.insets = new Insets(0, 0, 5, 5);
		gbc_lblWizardlevel.gridx = 4;
		gbc_lblWizardlevel.gridy = 0;
		panel.add(lblWizardlevel, gbc_lblWizardlevel);

		JLabel lblEnergy = new JLabel("Energy");
		GridBagConstraints gbc_lblEnergy = new GridBagConstraints();
		gbc_lblEnergy.insets = new Insets(0, 0, 5, 5);
		gbc_lblEnergy.gridx = 0;
		gbc_lblEnergy.gridy = 2;
		panel.add(lblEnergy, gbc_lblEnergy);

		lblEnergyamount = new JLabel("EnergyAmount");
		GridBagConstraints gbc_lblEnergyamount = new GridBagConstraints();
		gbc_lblEnergyamount.insets = new Insets(0, 0, 5, 5);
		gbc_lblEnergyamount.gridx = 1;
		gbc_lblEnergyamount.gridy = 2;
		panel.add(lblEnergyamount, gbc_lblEnergyamount);

		lblEnergytime = new JLabel("EnergyTime");
		GridBagConstraints gbc_lblEnergytime = new GridBagConstraints();
		gbc_lblEnergytime.insets = new Insets(0, 0, 5, 5);
		gbc_lblEnergytime.gridx = 2;
		gbc_lblEnergytime.gridy = 2;
		panel.add(lblEnergytime, gbc_lblEnergytime);

		JLabel lblDimesionalRifts = new JLabel("Dimesional Rifts");
		GridBagConstraints gbc_lblDimesionalRifts = new GridBagConstraints();
		gbc_lblDimesionalRifts.insets = new Insets(0, 0, 5, 5);
		gbc_lblDimesionalRifts.gridx = 5;
		gbc_lblDimesionalRifts.gridy = 2;
		panel.add(lblDimesionalRifts, gbc_lblDimesionalRifts);

		lblRiftcrystalamount = new JLabel("RiftCrystalAmount");
		GridBagConstraints gbc_lblRiftcrystalamount = new GridBagConstraints();
		gbc_lblRiftcrystalamount.insets = new Insets(0, 0, 5, 0);
		gbc_lblRiftcrystalamount.gridx = 6;
		gbc_lblRiftcrystalamount.gridy = 2;
		panel.add(lblRiftcrystalamount, gbc_lblRiftcrystalamount);

		JLabel lblWings = new JLabel("Wings");
		GridBagConstraints gbc_lblWings = new GridBagConstraints();
		gbc_lblWings.insets = new Insets(0, 0, 5, 5);
		gbc_lblWings.gridx = 0;
		gbc_lblWings.gridy = 3;
		panel.add(lblWings, gbc_lblWings);

		lblWingsamount = new JLabel("WingsAmount");
		GridBagConstraints gbc_lblWingsamount = new GridBagConstraints();
		gbc_lblWingsamount.insets = new Insets(0, 0, 5, 5);
		gbc_lblWingsamount.gridx = 1;
		gbc_lblWingsamount.gridy = 3;
		panel.add(lblWingsamount, gbc_lblWingsamount);

		lblWingstime = new JLabel("WingsTime");
		GridBagConstraints gbc_lblWingstime = new GridBagConstraints();
		gbc_lblWingstime.insets = new Insets(0, 0, 5, 5);
		gbc_lblWingstime.gridx = 2;
		gbc_lblWingstime.gridy = 3;
		panel.add(lblWingstime, gbc_lblWingstime);

		JLabel lblSocialPoints = new JLabel("Social Points");
		GridBagConstraints gbc_lblSocialPoints = new GridBagConstraints();
		gbc_lblSocialPoints.insets = new Insets(0, 0, 5, 5);
		gbc_lblSocialPoints.gridx = 5;
		gbc_lblSocialPoints.gridy = 3;
		panel.add(lblSocialPoints, gbc_lblSocialPoints);

		lblSocialpointamount = new JLabel("SocialPointAmount");
		GridBagConstraints gbc_lblSocialpointamount = new GridBagConstraints();
		gbc_lblSocialpointamount.insets = new Insets(0, 0, 5, 0);
		gbc_lblSocialpointamount.gridx = 6;
		gbc_lblSocialpointamount.gridy = 3;
		panel.add(lblSocialpointamount, gbc_lblSocialpointamount);

		JLabel lblMana = new JLabel("Mana");
		GridBagConstraints gbc_lblMana = new GridBagConstraints();
		gbc_lblMana.insets = new Insets(0, 0, 5, 5);
		gbc_lblMana.gridx = 0;
		gbc_lblMana.gridy = 4;
		panel.add(lblMana, gbc_lblMana);

		lblManaamount = new JLabel("ManaAmount");
		GridBagConstraints gbc_lblManaamount = new GridBagConstraints();
		gbc_lblManaamount.insets = new Insets(0, 0, 5, 5);
		gbc_lblManaamount.gridx = 1;
		gbc_lblManaamount.gridy = 4;
		panel.add(lblManaamount, gbc_lblManaamount);

		JLabel lblHonorPoints = new JLabel("Glory Points");
		GridBagConstraints gbc_lblHonorPoints = new GridBagConstraints();
		gbc_lblHonorPoints.insets = new Insets(0, 0, 5, 5);
		gbc_lblHonorPoints.gridx = 5;
		gbc_lblHonorPoints.gridy = 4;
		panel.add(lblHonorPoints, gbc_lblHonorPoints);

		lblHonorpointsamount = new JLabel("HonorPointsAmount");
		GridBagConstraints gbc_lblHonorpointsamount = new GridBagConstraints();
		gbc_lblHonorpointsamount.insets = new Insets(0, 0, 5, 0);
		gbc_lblHonorpointsamount.gridx = 6;
		gbc_lblHonorpointsamount.gridy = 4;
		panel.add(lblHonorpointsamount, gbc_lblHonorpointsamount);

		JLabel lblCrystals = new JLabel("Crystals");
		GridBagConstraints gbc_lblCrystals = new GridBagConstraints();
		gbc_lblCrystals.insets = new Insets(0, 0, 0, 5);
		gbc_lblCrystals.gridx = 0;
		gbc_lblCrystals.gridy = 5;
		panel.add(lblCrystals, gbc_lblCrystals);

		lblCrystalsamount = new JLabel("CrystalsAmount");
		GridBagConstraints gbc_lblCrystalsamount = new GridBagConstraints();
		gbc_lblCrystalsamount.insets = new Insets(0, 0, 0, 5);
		gbc_lblCrystalsamount.gridx = 1;
		gbc_lblCrystalsamount.gridy = 5;
		panel.add(lblCrystalsamount, gbc_lblCrystalsamount);

		JLabel lblGuildPoints = new JLabel("Guild Points");
		GridBagConstraints gbc_lblGuildPoints = new GridBagConstraints();
		gbc_lblGuildPoints.insets = new Insets(0, 0, 0, 5);
		gbc_lblGuildPoints.gridx = 5;
		gbc_lblGuildPoints.gridy = 5;
		panel.add(lblGuildPoints, gbc_lblGuildPoints);

		lblGuildpointamounts = new JLabel("GuildPointAmounts");
		GridBagConstraints gbc_lblGuildpointamounts = new GridBagConstraints();
		gbc_lblGuildpointamounts.gridx = 6;
		gbc_lblGuildpointamounts.gridy = 5;
		panel.add(lblGuildpointamounts, gbc_lblGuildpointamounts);

		panel_1 = new JPanel();
		panel_1.setLayout(new BorderLayout());
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 1;
		gbc_panel_1.gridy = 0;
		add(panel_1, gbc_panel_1);

		textArea = new JTextArea();
		textArea.setEditable(false);

		JScrollPane scrollPanel = new JScrollPane(textArea);
		panel_1.add(scrollPanel, BorderLayout.CENTER);
	}

	private String secondsToTime(int seconds) {
		int min = seconds / 60;
		int sec = seconds - min * 60;

		return min + ":" + (sec < 10 ? "0" : "") + sec;
	}

	private void updateHtml() {
		if (lblWizardname.getText().equalsIgnoreCase("WizardName")) {
			return;
		}

		String baseHtmlFilename = "base.html";
		String htmlFilename = lblWizardname.getText() + ".html";

		String baseHtml = "";

		try {
			byte[] encoded = Files.readAllBytes(Paths.get(baseHtmlFilename));
			baseHtml = new String(encoded, Charset.defaultCharset());
		} catch (Exception e) {
			e.printStackTrace();
		}

		String html = baseHtml;
		html = html.replaceFirst("'wizardname'", lblWizardname.getText() + " " + lblWizardlevel.getText());
		html = html.replaceFirst("'wizardlevel'", lblWizardlevel.getText());
		html = html.replaceFirst("'energy'", lblEnergyamount.getText());
		html = html.replaceFirst("'energytime'", lblEnergytime.getText());
		html = html.replaceFirst("'riftcrystals'", lblRiftcrystalamount.getText());
		html = html.replaceFirst("'wings'", lblWingsamount.getText());
		html = html.replaceFirst("'wingstime'", lblWingstime.getText());
		html = html.replaceFirst("'socialpoints'", lblSocialpointamount.getText());
		html = html.replaceFirst("'mana'", lblManaamount.getText());
		html = html.replaceFirst("'honorpoints'", lblHonorpointsamount.getText());
		html = html.replaceFirst("'crystals'", lblCrystalsamount.getText());
		html = html.replaceFirst("'guildpoints'", lblGuildpointamounts.getText());

		String crLf = "&nbsp;</p><p>";
		String line = "&nbsp;</p><hr /><p>";

		String output = "";

		for (String item : outputStrings) {
			if (item.length() == 0) {
				continue;
			}
			output += item.replaceAll("\n", crLf) + line;
		}

		html = html.replaceFirst("'output'", output);

		PrintWriter out;

		try {
			out = new PrintWriter(htmlFilename);
			out.print(html);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void updateJSONObject() {
		if (lblWizardname.getText().equalsIgnoreCase("WizardName")) {
			return;
		}

		String jsonFilename = lblWizardname.getText() + ".json";

		JSONObject jsonObject = new JSONObject();

		jsonObject.put("wizardname", lblWizardname.getText());
		jsonObject.put("wizardlevel", lblWizardlevel.getText());
		jsonObject.put("energy", lblEnergyamount.getText());
		jsonObject.put("energytime", lblEnergytime.getText());
		jsonObject.put("riftcrystals", lblRiftcrystalamount.getText());
		jsonObject.put("wings", lblWingsamount.getText());
		jsonObject.put("wingstime", lblWingstime.getText());
		jsonObject.put("socialpoints", lblSocialpointamount.getText());
		jsonObject.put("mana", lblManaamount.getText());
		jsonObject.put("honorpoints", lblHonorpointsamount.getText());
		jsonObject.put("crystals", lblCrystalsamount.getText());
		jsonObject.put("guildpoints", lblGuildpointamounts.getText());

		JSONArray jsonArray = new JSONArray();
		for (String item : outputStrings) {
			jsonArray.put(item);
		}
		jsonObject.put("outputs", jsonArray);

		PrintWriter out;

		try {
			out = new PrintWriter(jsonFilename);
			out.print(jsonObject.toString());
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void updateOutput(String string) {
		for (int i = 0; i < outputStringsLength - 1; i++) {
			outputStrings[i] = outputStrings[i + 1];
		}
		outputStrings[outputStringsLength - 1] = string;

		String strings = "";
		for (String s : outputStrings) {
			strings += s + "\n";
		}

		textArea.setText(strings);
		updateHtml();

		String filename = "evaluatedRunes.txt";
		
		if (!lblWizardname.getText().equalsIgnoreCase("WizardName")) {
			filename = lblWizardname.getText() + "_runes.txt";
		}
		
		
		Rune.outputEvaluatedRunes(filename);
		

	}

	public void updateStats(Packet packet) {
		JSONObject wizardStats = packet.getWizardJson();
		if (wizardStats == null) {
			return;
		}

		String wizardName = wizardStats.getString("wizard_name");
		int wingsNextGain = wizardStats.getInt("arena_energy_next_gain");
		int level = wizardStats.getInt("wizard_level");
		int riftCrystals = wizardStats.getInt("darkportal_energy");
		int riftCrystalsMax = wizardStats.getInt("darkportal_energy_max");
		int energyNextGain = wizardStats.getInt("next_energy_gain");
		int energy = wizardStats.getInt("wizard_energy");
		int wings = wizardStats.getInt("arena_energy");
		int socialPoints = wizardStats.getInt("social_point_current");
		int socialPointsMax = wizardStats.getInt("social_point_max");
		int wingsMax = wizardStats.getInt("arena_energy_max");
		int gloryPoints = wizardStats.getInt("honor_point");
		int energyMax = wizardStats.getInt("energy_max");
		int mana = wizardStats.getInt("wizard_mana");
		int crystals = wizardStats.getInt("wizard_crystal");
		int guildPoints = wizardStats.getInt("guild_point");

		lblWizardname.setText(wizardName);
		lblWizardlevel.setText("Lvl. " + level);
		lblEnergyamount.setText(energy + " / " + energyMax);
		lblEnergytime.setText(secondsToTime(energyNextGain));
		lblRiftcrystalamount.setText(riftCrystals + " / " + riftCrystalsMax);
		lblWingsamount.setText(wings + " / " + wingsMax);
		lblWingstime.setText(secondsToTime(wingsNextGain));
		lblSocialpointamount.setText(socialPoints + " / " + socialPointsMax);
		lblManaamount.setText(NumberFormat.getIntegerInstance().format(mana));
		lblHonorpointsamount.setText(NumberFormat.getIntegerInstance().format(gloryPoints));
		lblCrystalsamount.setText(NumberFormat.getIntegerInstance().format(crystals));
		lblGuildpointamounts.setText(NumberFormat.getIntegerInstance().format(guildPoints));

		// textArea.setText(packetTypes.toString());
		updateHtml();
	}

}
