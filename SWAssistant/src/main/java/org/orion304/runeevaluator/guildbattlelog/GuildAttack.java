package main.java.org.orion304.runeevaluator.guildbattlelog;

import org.json.JSONArray;
import org.json.JSONObject;

public class GuildAttack implements Comparable<GuildAttack> {

	private static final int padLength = 16;

	public static double getHealthAfterAttack(GuildAttack attack, double initialHealth) {
		return attack.getHealthAfterAttack(initialHealth);
	}

	public final String attacker, attackerGuild;
	public final String defender, defenderGuild;
	public final GuildBattleResult result1, result2;
	public final long battleEndTime;

	public final int guildPoints;

	public GuildAttack(JSONObject json) {
		attacker = formatName(json.getString("wizard_name"));
		attackerGuild = json.getString("guild_name");
		defender = formatName(json.getString("opp_wizard_name"));
		defenderGuild = json.getString("opp_guild_name");
		battleEndTime = json.getLong("battle_end");
		guildPoints = json.getInt("guild_point_var");

		JSONArray results = json.getJSONArray("result");
		result1 = GuildBattleResult.getById(results.getInt(0));
		result2 = GuildBattleResult.getById(results.getInt(1));
	}

	@Override
	public int compareTo(GuildAttack arg0) {
		int result;

		result = Long.compare(battleEndTime, arg0.battleEndTime);
		if (result != 0) {
			return result;
		}

		return attacker.compareTo(arg0.attacker);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		GuildAttack other = (GuildAttack) obj;
		if (attacker == null) {
			if (other.attacker != null) {
				return false;
			}
		} else if (!attacker.equals(other.attacker)) {
			return false;
		}
		if (attackerGuild == null) {
			if (other.attackerGuild != null) {
				return false;
			}
		} else if (!attackerGuild.equals(other.attackerGuild)) {
			return false;
		}
		if (battleEndTime != other.battleEndTime) {
			return false;
		}
		if (defender == null) {
			if (other.defender != null) {
				return false;
			}
		} else if (!defender.equals(other.defender)) {
			return false;
		}
		if (defenderGuild == null) {
			if (other.defenderGuild != null) {
				return false;
			}
		} else if (!defenderGuild.equals(other.defenderGuild)) {
			return false;
		}
		if (guildPoints != other.guildPoints) {
			return false;
		}
		if (result1 != other.result1) {
			return false;
		}
		if (result2 != other.result2) {
			return false;
		}
		return true;
	}

	private String formatName(String string) {
		return String.format("%1$-" + padLength + "s", string);
	}

	public double getHealthAfterAttack(double initialHealth) {
		double health = getHealthAfterAttack(initialHealth, result1);
		health = getHealthAfterAttack(health, result2);

		return health;
	}

	private double getHealthAfterAttack(double initialHealth, GuildBattleResult result) {
		double healthChange = initialHealth * result.HPChange;
		if (result == GuildBattleResult.WIN && healthChange < 0.1) {
			healthChange = 0.1;
		}

		return initialHealth - healthChange;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attacker == null) ? 0 : attacker.hashCode());
		result = prime * result + ((attackerGuild == null) ? 0 : attackerGuild.hashCode());
		result = prime * result + (int) (battleEndTime ^ (battleEndTime >>> 32));
		result = prime * result + ((defender == null) ? 0 : defender.hashCode());
		result = prime * result + ((defenderGuild == null) ? 0 : defenderGuild.hashCode());
		result = prime * result + guildPoints;
		result = prime * result + ((result1 == null) ? 0 : result1.hashCode());
		result = prime * result + ((result2 == null) ? 0 : result2.hashCode());
		return result;
	}

}
