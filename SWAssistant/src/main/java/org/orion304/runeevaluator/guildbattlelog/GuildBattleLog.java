package main.java.org.orion304.runeevaluator.guildbattlelog;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;

import org.json.JSONArray;
import org.json.JSONObject;

public class GuildBattleLog implements Comparable<GuildBattleLog> {

	private static final double redHealth = 30.0;
	private static final double yellowHealth = 50.0;

	public String guildName;
	private HashMap<String, Double> opponentHealths = new HashMap<>();
	private TreeSet<GuildAttack> attacks = new TreeSet<>();

	public final ArrayList<String> export = new ArrayList<>();
	public final ArrayList<String> exportLong = new ArrayList<>();
	public String exportHeader = null;
	private Date firstAttackTime = null;

	public GuildBattleLog(JSONObject json) {
		guildName = json.getJSONObject("opp_guild_info").getString("name");
		guildName = guildName.replaceAll("\\*", "");
		guildName = guildName.replaceAll("\\?", "");
		JSONArray battleLog = json.getJSONArray("battle_log_list");
		for (int j = 0; j < battleLog.length(); j++) {
			JSONObject jsonObject = battleLog.getJSONObject(j);
			attacks.add(new GuildAttack(jsonObject));
		}

		double initialHealth;
		double finalHealth;
		String string = "";
		String stringLong = "";

		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		DateFormat headerFormat = new SimpleDateFormat("MM-dd a");

		for (GuildAttack attack : attacks) {
			if (!opponentHealths.containsKey(attack.defender)) {
				opponentHealths.put(attack.defender, 100.0);
			}
		}

		for (GuildAttack attack : attacks) {

			initialHealth = opponentHealths.get(attack.defender);
			finalHealth = attack.getHealthAfterAttack(initialHealth);

			Calendar endTime = new GregorianCalendar();
			endTime.setTimeInMillis(attack.battleEndTime * 1000);

			if (exportHeader == null) {
				firstAttackTime = endTime.getTime();
				exportHeader = headerFormat.format(firstAttackTime) + " - " + guildName;
				export.add(exportHeader);
				exportLong.add(exportHeader);

				export.add("Attacker        \tDefender        \tInitial HP\tFinal HP\tTime        \tGuild HP");
				exportLong.add(
						"Attacker        \tDefender        \tInitial HP\tFinal HP\tTime        \t1st\t2nd\tGP\tGuild HP");
				// export.add("Attacker \tDefender \tInitial HP\tFinal
				// HP\tTime");
				// exportLong.add("Attacker \tDefender \tInitial HP\tFinal
				// HP\tTime \t1st\t2nd\tGP");
			}

			string = attack.attacker + "\t" + attack.defender + "\t" + formatHP(initialHealth) + "\t"
					+ formatHP(finalHealth) + "\t" + dateFormat.format(endTime.getTime());

			// string = attack.attacker + "\t" + attack.defender + "\t" +
			// formatHP(initialHealth) + "\t"
			// + formatHP(finalHealth) + "\t" +
			// dateFormat.format(endTime.getTime());

			stringLong = string + "\t" + attack.result1 + "\t" + attack.result2 + "\t" + attack.guildPoints + "\t"
					+ formatHP(getGuildHP());
			string += "\t" + formatHP(getGuildHP());

			String flag = shouldBeFlagged(attack);
			if (flag != null) {
				string += "\t" + flag;
				stringLong += "\t" + flag;
			}

			export.add(string);
			exportLong.add(stringLong);

			opponentHealths.put(attack.defender, finalHealth);

		}

	}

	@Override
	public int compareTo(GuildBattleLog arg0) {
		if (firstAttackTime == null) {
			return 1;
		}

		if (arg0.firstAttackTime == null) {
			return -1;
		}

		return firstAttackTime.compareTo(arg0.firstAttackTime);
	}

	private String formatHP(double HP) {
		return String.format("%1$-10s", (int) HP + "%");
	}

	private double getGuildHP() {
		double sum = 0;
		for (String opponent : opponentHealths.keySet()) {
			sum += opponentHealths.get(opponent);
		}

		sum /= opponentHealths.size();

		return sum;
	}

	private String shouldBeFlagged(GuildAttack attack) {
		double initialHealth = opponentHealths.get(attack.defender);

		HashSet<String> greenOpponents = new HashSet<>();
		HashSet<String> yellowOpponents = new HashSet<>();

		for (String opponent : opponentHealths.keySet()) {
			double health = opponentHealths.get(opponent);
			if (health > yellowHealth) {
				greenOpponents.add(opponent);
			} else if (health > redHealth) {
				yellowOpponents.add(opponent);
			}
		}

		for (GuildAttack otherAttacks : attacks) {

			if (otherAttacks.attacker.equalsIgnoreCase(attack.attacker)) {
				if (greenOpponents.contains(otherAttacks.defender)) {
					greenOpponents.remove(otherAttacks.defender);
				}

				if (yellowOpponents.contains(otherAttacks.defender)) {
					yellowOpponents.remove(otherAttacks.defender);
				}
			}

		}

		String returnString = "";

		if (initialHealth <= redHealth && (greenOpponents.size() > 0 || yellowOpponents.size() > 0)) {
			// Fought a red when there were yellow or greens
			returnString = "Attacked a red when ";
			int i = 1;
			for (String string : greenOpponents) {
				returnString += string.trim();
				if (i < greenOpponents.size() - 1) {
					returnString += ", ";
				} else if (i < greenOpponents.size()) {
					returnString += " and ";
				}
				i++;
			}
			if (greenOpponents.size() == 1) {
				returnString += " was green";
				if (yellowOpponents.size() > 0) {
					returnString += " and ";
				}
			} else if (greenOpponents.size() > 1) {
				returnString += " were green";
				if (yellowOpponents.size() > 0) {
					returnString += " and ";
				}
			}

			i = 1;
			for (String string : yellowOpponents) {
				returnString += string.trim();
				if (i < yellowOpponents.size() - 1) {
					returnString += ", ";
				} else if (i < yellowOpponents.size()) {
					returnString += " and ";
				}
				i++;
			}

			if (yellowOpponents.size() == 1) {
				returnString += " was yellow";
			} else if (yellowOpponents.size() > 1) {
				returnString += " were yellow";
			}
			return returnString;
		} else if (initialHealth <= yellowHealth && greenOpponents.size() > 0) {
			// Fought a yellow when there were greens
			returnString = "Attacked a yellow when ";
			int i = 1;
			for (String string : greenOpponents) {
				returnString += string.trim();
				if (i < greenOpponents.size() - 1) {
					returnString += ", ";
				} else if (i < greenOpponents.size()) {
					returnString += " and ";
				}
				i++;
			}
			if (greenOpponents.size() == 1) {
				returnString += " was green";
			} else if (greenOpponents.size() > 1) {
				returnString += " were green";
			}
			return returnString;
		}

		return null;
	}

}
