package main.java.org.orion304.runeevaluator.guildbattlelog;

import java.util.HashMap;

public enum GuildBattleResult {

	WIN(1, .3), DRAW(3, .05), LOSE(2, 0);

	private static HashMap<Integer, GuildBattleResult> guildBattleResultById = new HashMap<>();

	static {
		for (GuildBattleResult value : values()) {
			guildBattleResultById.put(value.id, value);
		}
	}

	public static GuildBattleResult getById(int id) {
		if (guildBattleResultById.containsKey(id)) {
			return guildBattleResultById.get(id);
		}

		return null;
	}

	public final int id;
	public final double HPChange;

	GuildBattleResult(int id, double HPChange) {
		this.id = id;
		this.HPChange = HPChange;
	}

}
