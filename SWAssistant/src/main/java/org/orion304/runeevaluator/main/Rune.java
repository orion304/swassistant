package main.java.org.orion304.runeevaluator.main;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.json.JSONArray;
import org.json.JSONObject;

import main.java.org.orion304.runeevaluator.enums.AllStatType;
import main.java.org.orion304.runeevaluator.enums.RuneType;
import main.java.org.orion304.runeevaluator.enums.RuneWorth;
import main.java.org.orion304.runeevaluator.enums.StatType;

public class Rune {

	private static final HashSet<String> runeTypes = new HashSet<>();

	public static final ArrayList<Rune> runes = new ArrayList<>();

	private static final double averageIncrease = 6.0 / StatType.HP.maxStat * SWAssistantProperties.multiplier;

	private static final Double[][][] nr = new Double[5][5][4];
	private static final Double[] powerupRates = new Double[15];
	private static final Integer[][] powerupCosts = new Integer[15][7];

	// constants for representing the value added by +15ing a 6 star vs a 5 star
	// vs a 4 star
	private static final HashMap<StatType, Double> fiveToFourStar = new HashMap<>();
	private static final HashMap<StatType, Double> fiveToSixStar = new HashMap<>();

	static {
		for (StatType statType : StatType.values()) {
			double fiveToFour = 0;
			double fiveToSix = 0;

			switch (statType) {
			case HP:
			case ATK:
			case DEF:
				fiveToFour = -8;
				fiveToSix = 12;
				break;
			case RES:
			case ACC:
				fiveToFour = -7;
				fiveToSix = 13;
				break;
			case SPD:
				fiveToFour = -9;
				fiveToSix = 3;
				break;
			case CR:
				fiveToFour = -5;
				fiveToSix = 9;
				break;
			case CD:
				fiveToFour = -7;
				fiveToSix = 15;
				break;
			}

			fiveToFour = fiveToFour / statType.maxStat * SWAssistantProperties.multiplier;
			fiveToSix = fiveToSix / statType.maxStat * SWAssistantProperties.multiplier;

			fiveToFourStar.put(statType, fiveToFour);
			fiveToSixStar.put(statType, fiveToSix);
		}

		nr[4][1] = new Double[] { .25, 0., 0., 0. };
		nr[4][2] = new Double[] { .3, .1, 0., 0. };
		nr[4][3] = new Double[] { .3, .15, .05, 0. };
		nr[4][4] = new Double[] { .286, .171, .086, .029 };
		nr[3][1] = new Double[] { .333, 0., 0., 0. };
		nr[3][2] = new Double[] { .3, .167, 0., 0. };
		nr[3][3] = new Double[] { .3, .2, .1, 0. };
		nr[2][1] = new Double[] { .5, 0., 0., 0. };
		nr[2][2] = new Double[] { .3, .3, 0., 0. };
		nr[1][1] = new Double[] { 1., 0., 0., 0. };

		powerupRates[0] = 1D;
		powerupRates[1] = 1D;
		powerupRates[2] = 1D;
		powerupRates[3] = .85;
		powerupRates[4] = .70;
		powerupRates[5] = .60;
		powerupRates[6] = .50;
		powerupRates[7] = .40;
		powerupRates[8] = .30;
		powerupRates[9] = .20;
		powerupRates[10] = .15;
		powerupRates[11] = .10;
		powerupRates[12] = .05;
		powerupRates[13] = .03;
		powerupRates[14] = .02;

		powerupCosts[0][1] = 100;
		powerupCosts[0][2] = 150;
		powerupCosts[0][3] = 225;
		powerupCosts[0][4] = 330;
		powerupCosts[0][5] = 500;
		powerupCosts[0][6] = 720;
		powerupCosts[1][1] = 175;
		powerupCosts[1][2] = 300;
		powerupCosts[1][3] = 475;
		powerupCosts[1][4] = 680;
		powerupCosts[1][5] = 950;
		powerupCosts[1][6] = 1475;
		powerupCosts[2][1] = 250;
		powerupCosts[2][2] = 450;
		powerupCosts[2][3] = 725;
		powerupCosts[2][4] = 1030;
		powerupCosts[2][5] = 1400;
		powerupCosts[2][6] = 2200;
		powerupCosts[3][1] = 400;
		powerupCosts[3][2] = 700;
		powerupCosts[3][3] = 1075;
		powerupCosts[3][4] = 1480;
		powerupCosts[3][5] = 1925;
		powerupCosts[3][6] = 3050;
		powerupCosts[4][1] = 550;
		powerupCosts[4][2] = 950;
		powerupCosts[4][3] = 1425;
		powerupCosts[4][4] = 1930;
		powerupCosts[4][5] = 2450;
		powerupCosts[4][6] = 3900;
		powerupCosts[5][1] = 775;
		powerupCosts[5][2] = 1275;
		powerupCosts[5][3] = 1875;
		powerupCosts[5][4] = 2455;
		powerupCosts[5][5] = 3175;
		powerupCosts[5][6] = 4875;
		powerupCosts[6][1] = 1000;
		powerupCosts[6][2] = 1600;
		powerupCosts[6][3] = 2325;
		powerupCosts[6][4] = 2980;
		powerupCosts[6][5] = 3900;
		powerupCosts[6][6] = 5850;
		powerupCosts[7][1] = 1300;
		powerupCosts[7][2] = 2025;
		powerupCosts[7][3] = 2850;
		powerupCosts[7][4] = 3680;
		powerupCosts[7][5] = 4750;
		powerupCosts[7][6] = 6975;
		powerupCosts[8][1] = 1600;
		powerupCosts[8][2] = 2450;
		powerupCosts[8][3] = 3375;
		powerupCosts[8][4] = 4380;
		powerupCosts[8][5] = 5600;
		powerupCosts[8][6] = 8100;
		powerupCosts[9][1] = 2000;
		powerupCosts[9][2] = 3000;
		powerupCosts[9][3] = 4075;
		powerupCosts[9][4] = 5205;
		powerupCosts[9][5] = 6600;
		powerupCosts[9][6] = 9350;
		powerupCosts[10][1] = 2400;
		powerupCosts[10][2] = 3550;
		powerupCosts[10][3] = 4775;
		powerupCosts[10][4] = 6030;
		powerupCosts[10][5] = 7600;
		powerupCosts[10][6] = 10600;
		powerupCosts[11][1] = 2925;
		powerupCosts[11][2] = 4225;
		powerupCosts[11][3] = 5600;
		powerupCosts[11][4] = 6980;
		powerupCosts[11][5] = 8850;
		powerupCosts[11][6] = 11975;
		powerupCosts[12][1] = 3450;
		powerupCosts[12][2] = 4900;
		powerupCosts[12][3] = 6425;
		powerupCosts[12][4] = 7930;
		powerupCosts[12][5] = 10100;
		powerupCosts[12][6] = 13350;
		powerupCosts[13][1] = 4100;
		powerupCosts[13][2] = 5700;
		powerupCosts[13][3] = 7375;
		powerupCosts[13][4] = 9130;
		powerupCosts[13][5] = 11600;
		powerupCosts[13][6] = 14850;
		powerupCosts[14][1] = 4750;
		powerupCosts[14][2] = 6500;
		powerupCosts[14][3] = 8325;
		powerupCosts[14][4] = 10330;
		powerupCosts[14][5] = 13100;
		powerupCosts[14][6] = 16350;

	}

	public static void clearRunes() {
		runes.clear();
	}

	public static String getRuneTable() {
		StringBuilder builder = new StringBuilder();

		builder.append("ID	Equipped to monster	Rune set	Slot No	Stars	level	Sell Price	Primary effect	Prefix effect	First Substat	Second Substat	Third Substat	Fourth Substat	Best Value	Best Monster	Best Upg Value	Best Upg Monster	Mons	Ups	Type	All Worth	All Monsters	Rank	Inv Worth	Inventory Only	Rank	SPD fixed	Powerup Mana	+12 mana	+15 mana	Highest Speed	Highest Crit	Crit Fixed	Percentile\r\n");
		
		for (Rune rune : Rune.runes) {
			builder.append(rune.runeId);
			builder.append("\t");
			builder.append(rune.equippedTo);
			builder.append("\t");
			builder.append(rune.runeType.toString());
			builder.append("\t");
			builder.append(rune.slot);
			builder.append("\t");
			builder.append(rune.stars);
			builder.append("\t");
			builder.append(rune.level);
			builder.append("\t");
			builder.append(rune.sellPrice);
			builder.append("\t");
			for (String effect : rune.effects) {
				builder.append(effect);
				builder.append("\t");
			}

			builder.append(Double.parseDouble(DoubleRenderer.df.format(rune.bestRuneWorth).replaceAll(",", ".")));
			builder.append("\t");
			builder.append(rune.bestRuneMonster);
			builder.append("\t");
			builder.append(Double.parseDouble(DoubleRenderer.df.format(rune.bestUpgRuneWorth).replaceAll(",", ".")));
			builder.append("\t");
			builder.append(rune.bestUpgRuneMonster);
			builder.append("\t");
			builder.append(rune.runeWorth.size());
			builder.append("\t");

			builder.append(rune.powerups);
			builder.append("\t");
			builder.append(rune.statTypes.get(0) == null ? "" : rune.statTypes.get(0).toString());
			builder.append("\t");
			builder.append(rune.getWorth(false));
			builder.append("\t");
			builder.append(rune.getEquippedMonsters(false));
			builder.append("\t");
			builder.append(rune.bestRank);
			builder.append("\t");
			builder.append(rune.getWorth(true));
			builder.append("\t");
			builder.append(rune.getEquippedMonsters(true));
			builder.append("\t");
			builder.append(rune.bestRankInventoryOnly); // fix
			builder.append("\t");
			builder.append(rune.speedFixed);
			builder.append("\t");
			builder.append(rune.getManaForPowerups());
			builder.append("\t");
			builder.append(rune.getManaTo12());
			builder.append("\t");
			builder.append(rune.getManaTo15());
			builder.append("\t");
			builder.append(rune.highestSpeed);
			builder.append("\t");
			builder.append(rune.highestCritRate);
			builder.append("\t");
			builder.append(rune.critFixed);
			builder.append("\t");
			builder.append(Double.parseDouble(DoubleRenderer.df.format(rune.percentile).replaceAll(",", ".")));
			builder.append("\r\n");
		}

		return builder.toString();
	}

	public static void outputEvaluatedRunes(String filename) {
		String data = getRuneTable();
		PrintWriter out;
		try {
			out = new PrintWriter(filename);
			out.print(data);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void reevaluateRunes() {
		Monster.clearMonsterRunes();

		for (Rune rune : runes) {
			rune.evaluateRune();
		}

		for (Rune rune : runes) {
			rune.pickBestMonster();
		}
	}

	public static void removeRune(Rune rune) {
		runes.remove(rune);
	}

	public final int runeId;

	public String equippedTo;
	public boolean inInventory;

	public RuneType runeType;

	public final int slot;
	public final int stars;
	public final int level;
	public final int sellPrice;
	public final List<String> effects = new ArrayList<>();
	public final List<AllStatType> allStatTypes = new ArrayList<>();
	public final List<StatType> statTypes = new ArrayList<>();
	public final List<Integer> statValues = new ArrayList<>();
	public int powerups;
	public int numberOfSubstats;
	public HashMap<Monster, Double> runeWorth = new HashMap<>();
	public HashMap<Monster, Double> upgRuneWorth = new HashMap<>();
	public double bestRuneWorth = -1;
	public String bestRuneMonster = "";
	public double bestUpgRuneWorth = -1;
	public String bestUpgRuneMonster = "";
	public boolean locked = false;
	public int bestRank = 0;
	public int bestRankInventoryOnly = 0;
	public boolean speedFixed = false;
	public int highestSpeed = 0;
	public boolean critFixed = false;
	public int highestCritRate = 0;
	public double percentile = 1000;

	public Rune(JSONObject jsonRune) {
		runeId = jsonRune.getInt("id");
		if (jsonRune.has("monster_n")) {
			equippedTo = jsonRune.getString("monster_n");
		}
		inInventory = equippedTo.equalsIgnoreCase("Inventory");
		// inInventory = jsonRune.getInt("locked") != 1;
		try {
			runeType = RuneType.valueOf(jsonRune.getString("set").toUpperCase());
		} catch (Exception e) {
			String t = jsonRune.getString("set").toUpperCase();
			if (!runeTypes.contains(t)) {
				runeTypes.add(t);
				System.out.println(t);
			}

			runeType = RuneType.ANY;
		}

		if (runeType == RuneType.ANY) {
			slot = 0;
			stars = 6;
			level = 0;
			sellPrice = 0;
			for (int i = 0; i < 6; i++) {
				String effect = "0";
				effects.add(effect);
				statTypes.add(null);
				statValues.add(0);
			}
		} else {
			slot = jsonRune.getInt("slot");
			stars = jsonRune.getInt("grade");
			level = jsonRune.getInt("level");
			sellPrice = 0;
			String effectType;
			StatType statType = null;
			int effectValue;
			String key = "";
			for (int i = 0; i < 6; i++) {
				switch (i) {
				case 0:
					key = "m";
					break;
				case 1:
					key = "i";
					break;
				case 2:
				case 3:
				case 4:
				case 5:
					key = "s" + (i - 1);
				}

				effectType = jsonRune.getString(key + "_t");
				if (effectType != "") {
					effectValue = jsonRune.getInt(key + "_v");
					if (effectValue == 0) {
						effects.add("");
					} else {
						effects.add(effectType + " " + effectValue);
					}
					statType = getStatType(effectType);
					statTypes.add(statType);
					if (statType != null) {
						statValues.add(effectValue);
					} else {
						statValues.add(0);
					}

				}
			}

			locked = jsonRune.getInt("locked") == 1;
		}

		evaluateRune();
		runes.add(this);
	}

	public Rune(JSONObject jsonRune, boolean addToRunes) {
		runeId = jsonRune.getInt("rune_id");
		equippedTo = "Inventory";
		inInventory = equippedTo.equalsIgnoreCase("Inventory");
		try {
			runeType = RuneType.getById(jsonRune.getInt("set_id"));
		} catch (Exception e) {
			String t = jsonRune.getString("set").toUpperCase();
			if (!runeTypes.contains(t)) {
				runeTypes.add(t);
				System.out.println(t);
			}

			runeType = RuneType.ANY;
		}

		if (runeType == RuneType.ANY) {
			slot = 0;
			stars = 6;
			level = 0;
			sellPrice = 0;
			for (int i = 0; i < 6; i++) {
				String effect = "0";
				effects.add(effect);
				statTypes.add(null);
				statValues.add(0);
			}
		} else {
			slot = jsonRune.getInt("slot_no");
			stars = jsonRune.getInt("class");
			level = jsonRune.getInt("upgrade_curr");
			sellPrice = jsonRune.getInt("sell_value");

			JSONArray pri = jsonRune.getJSONArray("pri_eff");
			JSONArray pre = jsonRune.getJSONArray("prefix_eff");
			JSONArray sec = jsonRune.getJSONArray("sec_eff");

			addStatByJsonArray(pri);
			addStatByJsonArray(pre);

			for (int i = 0; i < 4; i++) {
				if (sec.length() >= i + 1) {
					addStatByJsonArray(sec.getJSONArray(i));
				} else {
					statTypes.add(null);
					statValues.add(0);
					effects.add("");
				}
			}

			locked = false;
		}

		evaluateRune();
		if (addToRunes) {
			runes.add(this);
		}
	}

	public Rune(JSONObject jsonRune, String equippedTo) {
		this(jsonRune, true);
		this.equippedTo = equippedTo;
		inInventory = equippedTo.equalsIgnoreCase("Inventory");
	}

	public Rune(String[] rawRuneData) {
		runeId = Integer.parseInt(rawRuneData[0]);
		equippedTo = rawRuneData[1];
		inInventory = equippedTo.equalsIgnoreCase("0");
		if (rawRuneData[2].equalsIgnoreCase("???")) {
			runeType = RuneType.ANY;
			slot = 0;
			stars = 6;
			level = 0;
			sellPrice = 0;
			for (int i = 0; i < 6; i++) {
				String effect = "0";
				effects.add(effect);
				statTypes.add(null);
				statValues.add(0);
			}
		} else {
			runeType = RuneType.valueOf(rawRuneData[2].toUpperCase());
			slot = Integer.parseInt(rawRuneData[3]);
			stars = Integer.parseInt(rawRuneData[4]);
			level = Integer.parseInt(rawRuneData[5]);
			sellPrice = Integer.parseInt(rawRuneData[6]);
			for (int i = 0; i < 6; i++) {
				String effect = rawRuneData[7 + i];
				effects.add(effect);
				statTypes.add(getStatType(effect));
				statValues.add(getStatValue(effect));
			}
		}
		evaluateRune();
		runes.add(this);
	}

	private void addStatByJsonArray(JSONArray stat) {
		AllStatType allStatType;
		StatType statType;
		int statValue;

		allStatType = AllStatType.getById(stat.getInt(0));
		statType = allStatType == null ? null : allStatType.statType;
		statValue = stat.getInt(1);

		allStatTypes.add(allStatType);
		statTypes.add(statType);
		statValues.add(statType == null ? 0 : statValue);

		effects.add(allStatType == null ? "" : allStatType + " " + statValue);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Rune other = (Rune) obj;
		if (runeId != other.runeId) {
			return false;
		}
		return true;
	}

	private void evaluateRune() {
		if (!inInventory) {
			// return;
		}

		numberOfSubstats = 0;

		// 0 - primary, 1 - static, 2-5 - subs
		// Do some counting of the types of subs here
		StatType statType;
		for (int i = 0; i < 6; i++) {
			statType = statTypes.get(i);
			if (i > 1 && !effects.get(i).isEmpty()) {
				numberOfSubstats++;
			}

			if (statType == null) {
				continue;
			}
		}

		// Evaluate the rune itself
		HashMap<RuneWorth, Double> staticStats = new HashMap<>();
		HashMap<RuneWorth, Double> stat1 = new HashMap<>();
		HashMap<RuneWorth, Double> stat2 = new HashMap<>();
		HashMap<RuneWorth, Double> stat3 = new HashMap<>();
		HashMap<RuneWorth, Double> stat4 = new HashMap<>();

		ArrayList<HashMap<RuneWorth, Double>> allStats = new ArrayList<>();
		allStats.add(staticStats);
		allStats.add(stat1);
		allStats.add(stat2);
		allStats.add(stat3);
		allStats.add(stat4);

		HashMap<RuneWorth, Double> staticStatsUpg = new HashMap<>();
		HashMap<RuneWorth, Double> stat1Upg = new HashMap<>();
		HashMap<RuneWorth, Double> stat2Upg = new HashMap<>();
		HashMap<RuneWorth, Double> stat3Upg = new HashMap<>();
		HashMap<RuneWorth, Double> stat4Upg = new HashMap<>();

		ArrayList<HashMap<RuneWorth, Double>> allStatsUpg = new ArrayList<>();
		allStatsUpg.add(staticStatsUpg);
		allStatsUpg.add(stat1Upg);
		allStatsUpg.add(stat2Upg);
		allStatsUpg.add(stat3Upg);
		allStatsUpg.add(stat4Upg);

		double statValue, upgStatValue;

		powerups = level / 3;
		powerups = numberOfSubstats - powerups;

		if (powerups < 0) {
			powerups = 0;
		}

		for (int i = 1; i < 6; i++) {
			statType = statTypes.get(i);
			if (statType == null) {
				continue;
			}
			statValue = getNormalizedStatValue(i);
			upgStatValue = statValue;

			if (statType == StatType.SPD) {
				if (i == 1 || powerups == 0) {
					speedFixed = true;
				}
				highestSpeed = statValues.get(i);
				highestSpeed = highestSpeed + (speedFixed ? 0 : stars * powerups);
			}

			if (statType == StatType.CR) {
				if (i == 1 || powerups == 0) {
					critFixed = true;
				}
				highestCritRate = statValues.get(i);
				highestCritRate = highestCritRate + (critFixed ? 0 : stars * powerups);
			}

			if (powerups > 0) {
				Double[] probabilities = nr[numberOfSubstats][powerups];
				double avgValueIncrease = (probabilities[0] + probabilities[1] * 2 + probabilities[2] * 3
						+ probabilities[3] * 4) * averageIncrease;
				if (stars == 5) {
					avgValueIncrease *= .8;
				} else if (stars == 4) {
					avgValueIncrease *= .7;
				}
				upgStatValue += avgValueIncrease;
			}

			HashMap<RuneWorth, Double> currentStats = allStats.get(i - 1);
			HashMap<RuneWorth, Double> currentStatsUpg = allStatsUpg.get(i - 1);

			for (RuneWorth runeWorth : RuneWorth.values()) {
				currentStats.put(runeWorth, statValue * runeWorth.multiplier);
				currentStatsUpg.put(runeWorth, upgStatValue * runeWorth.multiplier);
			}
		}

		if (numberOfSubstats == 4 && highestSpeed == 0) {
			speedFixed = true;
		}

		if (numberOfSubstats == 4 && highestCritRate == 0) {
			critFixed = true;
		}

		double value = 0;
		double upgValue = 0;

		Set<Monster> monsters;
		if (slot % 2 == 0) {
			// Slot is even, can constrain the monsters more
			monsters = Cache.monstersByRuneStatSlot.get(new RuneStatSlot(runeType, statTypes.get(0), slot));
		} else {
			monsters = Cache.monstersByRuneType.get(runeType);
		}

		if (monsters != null) {

			for (Monster monster : monsters) {

				value = 0;
				upgValue = 0;
				for (int i = 0; i < 5; i++) {
					statType = statTypes.get(i + 1);
					if (statType == null) {
						continue;
					}
					RuneWorth runeWorth = monster.getWorth(statType);
					value += allStats.get(i).get(runeWorth);
					upgValue += allStatsUpg.get(i).get(runeWorth);
				}

				double starAddedValue = 0;
				if (slot % 2 == 0) {
					if (stars == 4) {
						starAddedValue = fiveToFourStar.get(statTypes.get(0));
					} else if (stars == 6) {
						starAddedValue = fiveToSixStar.get(statTypes.get(0));
					}
				}

				value += starAddedValue;
				upgValue += starAddedValue;

				runeWorth.put(monster, value);
				upgRuneWorth.put(monster, upgValue);

				if (value > bestRuneWorth) {
					bestRuneWorth = value;
					bestRuneMonster = monster.getName();
				}

				if (upgValue > bestUpgRuneWorth) {
					bestUpgRuneWorth = upgValue;
					bestUpgRuneMonster = monster.getName();
				}

				monster.addRune(this);

				if (inInventory) {
					monster.addRuneOnlyInventory(this);
				}

			}

			// for (Monster monster : monsters) {
			// double thisWorth = runeWorth.get(monster);
			// if (!atLeastOneGood || thisWorth > 0) {
			// monster.addRune(this);
			// } else {
			// runeWorth.remove(monster);
			// }
			// }
		}

	}

	public String getEquippedMonsters(boolean inventoryOnly) {
		String str = "";
		TreeSet<MonsterRuneRank> ranks = new TreeSet<>();
		for (Monster monster : runeWorth.keySet()) {
			ranks.add(
					new MonsterRuneRank(monster, monster.getRuneRank(this, inventoryOnly), upgRuneWorth.get(monster)));
		}

		int j = 0;
		for (MonsterRuneRank rank : ranks) {
			if (j == 0) {
				if (inventoryOnly) {
					bestRankInventoryOnly = rank.place;
				} else {
					bestRank = rank.place;
				}
			}
			j++;
			str += rank + ", ";
		}

		if (str != "") {
			str = str.substring(0, str.length() - 2);
		}

		if (runeWorth.size() > 0) {
			percentile = bestRank / (double) runeWorth.size() * 100.0;
		}

		return str;
	}

	public int getManaForPowerups() {
		return getManaToLevel(numberOfSubstats * 3);
	}

	public int getManaTo12() {
		return getManaToLevel(12);
	}

	public int getManaTo15() {
		return getManaToLevel(15);
	}

	public int getManaToLevel(int toLevel) {
		double manacost = 0;
		for (int i = level; i < toLevel; i++) {
			manacost += powerupCosts[i][stars] / powerupRates[i];
		}
		return (int) manacost;
	}

	private double getNormalizedStatValue(int index) {
		return getNormalizedStatValue(statTypes.get(index), statValues.get(index));
	}

	private double getNormalizedStatValue(StatType statType, double power) {
		if (stars < 4 || statType == null) {// || statType ==
											// StatType.WORTHLESS) {
			return 0;
		}

		double weight = 1;

		if (statType == StatType.CR || statType == StatType.CD) {
			weight = 1.5;
		}

		return power / statType.maxStat * SWAssistantProperties.multiplier * weight;
	}

	private StatType getStatType(String effect) {
		if ((!Main.useOptimizerIntegration && !effect.contains(" ")) || effect == "") {
			return null;
		}

		boolean isPercent = effect.contains("%");

		if (effect.contains("Accuracy") || effect.contains("ACC")) {
			return StatType.ACC;
		} else if (effect.contains("Resistance") || effect.contains("RES")) {
			return StatType.RES;
		} else if (effect.contains("CRI Dmg") || effect.contains("CDmg")) {
			return StatType.CD;
		} else if (effect.contains("CRI Rate") || effect.contains("CRate")) {
			return StatType.CR;
		} else if (effect.contains("SPD") || effect.contains("SPD")) {
			return StatType.SPD;
		} else if (effect.contains("HP")) {
			return isPercent ? StatType.HP : null;
		} else if (effect.contains("DEF")) {
			return isPercent ? StatType.DEF : null;
		} else if (effect.contains("ATK")) {
			return isPercent ? StatType.ATK : null;
		} else {
			return null; // StatType.WORTHLESS;
		}

	}

	private Integer getStatValue(String effect) {
		if (effect.contains("->")) {
			effect = effect.split("->")[0];
		}
		effect = effect.replaceAll("[^0-9]", "");
		try {
			return Integer.parseInt(effect);
		} catch (NumberFormatException e) {
			return 0;
		}
	}

	public int getWorth(boolean inventoryOnly) {
		TreeSet<MonsterRuneRank> ranks = new TreeSet<>();
		for (Monster monster : runeWorth.keySet()) {
			ranks.add(
					new MonsterRuneRank(monster, monster.getRuneRank(this, inventoryOnly), upgRuneWorth.get(monster)));
		}

		int worth = 0;
		int maxRank = 5;
		for (MonsterRuneRank rank : ranks) {
			if (rank.place > maxRank || rank.place == 0) {
				break;
			}
			worth += (maxRank - rank.place + 1);
		}

		return worth;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + runeId;
		return result;
	}

	private void pickBestMonster() {
		double worth;
		int n;
		int numberOfSimilarRunes = -1;
		for (Monster monster : runeWorth.keySet()) {
			worth = runeWorth.get(monster);
			if (worth < bestRuneWorth) {
				continue;
			}
			n = monster.getRunesBySlot(slot).size();
			if (numberOfSimilarRunes < 0 || n < numberOfSimilarRunes) {
				numberOfSimilarRunes = n;
				bestRuneMonster = monster.getName();
			}
		}

		numberOfSimilarRunes = -1;
		for (Monster monster : upgRuneWorth.keySet()) {
			worth = upgRuneWorth.get(monster);
			if (worth < bestUpgRuneWorth) {
				continue;
			}
			n = monster.getRunesBySlot(slot).size();
			if (numberOfSimilarRunes < 0 || n < numberOfSimilarRunes) {
				numberOfSimilarRunes = n;
				bestUpgRuneMonster = monster.getName();
			}
		}

	}

	@Override
	public String toString() {
		return "Rune [runeType=" + runeType + ", slot=" + slot + ", stars=" + stars + ", sellPrice=" + sellPrice
				+ ", bestRuneWorth=" + bestRuneWorth + ", bestRuneMonster=" + bestRuneMonster + ", bestUpgRuneWorth="
				+ bestUpgRuneWorth + ", bestUpgRuneMonster=" + bestUpgRuneMonster + ", bestRank=" + bestRank
				+ ", bestRankInventoryOnly=" + bestRankInventoryOnly + ", highestSpeed=" + highestSpeed
				+ ", highestCritRate=" + highestCritRate + "]";
	}

}
