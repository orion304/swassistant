package main.java.org.orion304.runeevaluator.main;

import org.json.JSONObject;

public class Packet {

	private String packetType;
	private String request;
	private String response;

	public JSONObject jsonRequest;
	public JSONObject jsonResponse;

	private JSONObject wizardJson = null;

	public Packet(String packetType, String request, String response) {
		this.packetType = packetType;
		this.request = request;
		this.response = response;

		jsonRequest = new JSONObject(this.request);

		jsonResponse = new JSONObject(this.response);
		if (jsonResponse.has("wizard_info")) {
			wizardJson = jsonResponse.getJSONObject("wizard_info");
		}
	}

	public String getPacketType() {
		return packetType;
	}

	public String getRequest() {
		return request;
	}

	public String getResponse() {
		return response;
	}

	public JSONObject getWizardJson() {
		return this.wizardJson;
	}

}
