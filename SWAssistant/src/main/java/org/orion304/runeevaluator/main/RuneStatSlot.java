package main.java.org.orion304.runeevaluator.main;

import main.java.org.orion304.runeevaluator.enums.RuneType;
import main.java.org.orion304.runeevaluator.enums.StatType;

public class RuneStatSlot {

	private final RuneType runeType;
	private final StatType statType;
	private final int slot;

	public RuneStatSlot(RuneType runeType, StatType statType, int slot) {
		this.runeType = runeType;
		this.statType = statType;
		this.slot = slot;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RuneStatSlot other = (RuneStatSlot) obj;
		if (runeType != other.runeType) {
			return false;
		}
		if (slot != other.slot) {
			return false;
		}
		if (statType != other.statType) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((runeType == null) ? 0 : runeType.hashCode());
		result = prime * result + slot;
		result = prime * result + ((statType == null) ? 0 : statType.hashCode());
		return result;
	}

}
