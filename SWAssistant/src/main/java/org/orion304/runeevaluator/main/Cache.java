package main.java.org.orion304.runeevaluator.main;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import main.java.org.orion304.runeevaluator.enums.RuneType;
import main.java.org.orion304.runeevaluator.enums.StatType;

public class Cache {

	public static HashMap<RuneType, HashSet<Monster>> monstersByRuneType = new HashMap<>();
	public static HashMap<RuneStatSlot, HashSet<Monster>> monstersByRuneStatSlot = new HashMap<>();

	public static void buildCache() {
		monstersByRuneType.clear();
		monstersByRuneStatSlot.clear();

		for (RuneType runeType : RuneType.values()) {
			monstersByRuneType.put(runeType, new HashSet<Monster>());

			for (StatType statType : StatType.values()) {
				for (int slot : new int[] { 2, 4, 6 }) {
					monstersByRuneStatSlot.put(new RuneStatSlot(runeType, statType, slot), new HashSet<Monster>());
				}
			}
		}

		RuneType set1, set2, set3;
		List<StatType> slot2, slot4, slot6;
		int slot;
		for (Monster monster : Monster.monsters) {
			set1 = monster.getSet1();
			set2 = monster.getSet2();
			set3 = monster.getSet3();

			slot2 = monster.getSlot2();
			slot4 = monster.getSlot4();
			slot6 = monster.getSlot6();

			for (RuneType set : monster.hasFlexibleSet() ? RuneType.values() : new RuneType[] { set1, set2, set3 }) {
				if (set == null) {
					continue;
				}
				monstersByRuneType.get(set).add(monster);

				slot = 2;
				for (List<StatType> slotTypes : Arrays.asList(slot2, slot4, slot6)) {
					for (StatType statType : slotTypes) {
						monstersByRuneStatSlot.get(new RuneStatSlot(set, statType, slot)).add(monster);
					}
					slot += 2;
				}

			}

		}
	}

}
