package main.java.org.orion304.runeevaluator.main;

import java.text.DecimalFormat;

import javax.swing.table.DefaultTableCellRenderer;

public class DoubleRenderer extends DefaultTableCellRenderer {

	public static final DecimalFormat df = new DecimalFormat("0.00");

	/**
	 *
	 */
	private static final long serialVersionUID = -3626019408040584805L;

	public DoubleRenderer() {
		super();
	}

	@Override
	public void setValue(Object value) {
		setText((value == null) ? "0" : df.format(value));
	}

}
