package main.java.org.orion304.runeevaluator.main;

import javax.swing.table.AbstractTableModel;

public class RuneTableModel extends AbstractTableModel {

	/**
	 *
	 */
	private static final long serialVersionUID = 8991206235752623974L;

	public static final String[] columns = { "Rune ID", "Equipped To", "Rune Set", "Slot", "Stars", "Level",
			"Sell Price", "Primary Stat", "Static Stat", "Sub 1", "Sub 2", "Sub 3", "Sub 4", "Best Value",
			"Best Monster", "Best Upg Value", "Best Upg Monster", "Monsters", "Powerups", "Stat Type", "All Worth",
			"All", "Rank", "Inv Worth", "Inventory Only", "Rank", "SPD Fixed", "Powerup Mana", "+12 Mana", "+15 Mana",
			"Max Speed", "Max Crit", "Crit Fixed", "Percentile" };

	private final int columnCount;
	private final int rowCount;
	private Object[][] data = new Object[Rune.runes.size()][columns.length];

	public RuneTableModel() {
		columnCount = columns.length;
		rowCount = Rune.runes.size();
		initializeData();
	}

	@Override
	public Class<?> getColumnClass(int col) {
		return getValueAt(0, col).getClass();
	}

	@Override
	public int getColumnCount() {
		return columnCount;
	}

	@Override
	public String getColumnName(int col) {
		return columns[col];
	}

	@Override
	public int getRowCount() {
		return rowCount;
	}

	@Override
	public Object getValueAt(int row, int col) {
		return data[row][col];
	}

	private void initializeData() {
		Rune.reevaluateRunes();

		int i = 0;
		for (Rune rune : Rune.runes) {
			data[i][0] = rune.runeId;
			data[i][1] = rune.equippedTo;
			data[i][2] = rune.runeType.toString();
			data[i][3] = rune.slot;
			data[i][4] = rune.stars;
			data[i][5] = rune.level;
			data[i][6] = rune.sellPrice;
			int j = 7;
			for (String effect : rune.effects) {
				data[i][j] = effect;
				j++;
			}

			data[i][j] = Double.parseDouble(DoubleRenderer.df.format(rune.bestRuneWorth).replaceAll(",", "."));
			j++;
			data[i][j] = rune.bestRuneMonster;
			j++;
			data[i][j] = Double.parseDouble(DoubleRenderer.df.format(rune.bestUpgRuneWorth).replaceAll(",", "."));
			j++;
			data[i][j] = rune.bestUpgRuneMonster;
			j++;
			data[i][j] = rune.runeWorth.size();
			j++;

			data[i][j] = rune.powerups;
			j++;
			data[i][j] = rune.statTypes.get(0) == null ? "" : rune.statTypes.get(0).toString();
			j++;
			data[i][j] = rune.getWorth(false);
			j++;
			data[i][j] = rune.getEquippedMonsters(false);
			j++;
			data[i][j] = rune.bestRank;
			j++;
			data[i][j] = rune.getWorth(true);
			j++;
			data[i][j] = rune.getEquippedMonsters(true);
			j++;
			data[i][j] = rune.bestRankInventoryOnly; // fix
			j++;
			data[i][j] = rune.speedFixed;
			j++;
			data[i][j] = rune.getManaForPowerups();
			j++;
			data[i][j] = rune.getManaTo12();
			j++;
			data[i][j] = rune.getManaTo15();
			j++;
			data[i][j] = rune.highestSpeed;
			j++;
			data[i][j] = rune.highestCritRate;
			j++;
			data[i][j] = rune.critFixed;
			j++;
			data[i][j] = Double.parseDouble(DoubleRenderer.df.format(rune.percentile).replaceAll(",", "."));
			j++;
			i++;
		}
	}

}
