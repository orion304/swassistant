package main.java.org.orion304.runeevaluator.main;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

public class Settings {

	// Of course it's castable this way
	@SuppressWarnings("unchecked")
	public static void readSettings(String filepath) {
		ObjectInputStream ois;
		ArrayList<Object> objects;
		Monster.monsters.clear();
		try {
			ois = new ObjectInputStream(new InflaterInputStream(new FileInputStream(filepath)));
			objects = (ArrayList<Object>) ois.readObject();
			ois.close();

			int meganCount = 2;
			for (Object object : objects) {
				if (object instanceof Monster) {
					Monster m = (Monster) object;
					if (m.getName().equalsIgnoreCase("Megan")) {
						meganCount++;
					}
					if (meganCount > 1) {
						Monster.monsters.add((Monster) object);
					}
				} else if (object instanceof FilePaths) {
					Main.filePaths = (FilePaths) object;
				}
			}
			System.out.println(Monster.getRuneDistribution());
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Main.filePaths = new FilePaths();
		}

	}

	public static void saveSettings(String filepath) {
		ObjectOutputStream oos;

		ArrayList<Object> settings = new ArrayList<>();
		settings.addAll(Monster.monsters);
		settings.add(Main.filePaths);

		try {
			oos = new ObjectOutputStream(new DeflaterOutputStream(new FileOutputStream(filepath)));
			oos.writeObject(settings);
			oos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
