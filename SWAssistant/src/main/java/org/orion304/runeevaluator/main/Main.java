package main.java.org.orion304.runeevaluator.main;

import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.Point;
import java.io.File;
import java.util.Random;

import javax.swing.JOptionPane;

import main.java.org.orion304.runeevaluator.enums.ImportType;
import main.java.org.orion304.runeevaluator.frames.EditMonsterFrame;
import main.java.org.orion304.runeevaluator.frames.ImportFrame;
import main.java.org.orion304.runeevaluator.frames.RuneDisplayFrame;

public class Main {

	public static boolean isWindows = false;

	private static final Random random = new Random();

	public static int xpos = 100;
	public static int ypos = 100;
	public static final int xsize = 1500;
	public static final int ysize = 900;

	private static Frame currentFrame = null;

	public static FilePaths filePaths;
	public static boolean useOptimizerIntegration = false;
	public static String optimizerJsonFilePath = "";

	public static void clearRunes() {
		Monster.clearMonsterRunes();
		Rune.clearRunes();
	}

	private static void closeCurrentFrame() {
		if (currentFrame != null) {
			currentFrame.setVisible(false);
			Point point = currentFrame.getLocation();
			xpos = (int) point.getX();
			ypos = (int) point.getY();
			currentFrame.dispose();
		}
	}

	public static void displayEditMonsterFrame(Monster monster) {
		closeCurrentFrame();
		EditMonsterFrame newFrame;
		newFrame = new EditMonsterFrame(monster);
		newFrame.setVisible(true);
		currentFrame = newFrame;

	}

	public static <T extends Frame> void displayFrame(Class<T> frameClass) {
		closeCurrentFrame();
		T newFrame;
		try {
			newFrame = frameClass.newInstance();
			newFrame.setVisible(true);
			currentFrame = newFrame;
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}

	}

	public static void displayImportFrame(ImportType importType) {
		closeCurrentFrame();
		ImportFrame newFrame;
		newFrame = new ImportFrame(importType);
		newFrame.setVisible(true);
		currentFrame = newFrame;

	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		if (System.getProperty("os.name").toLowerCase().contains("win")) {
			isWindows = true;
		}

		String username = System.getProperty("user.name");
		String packagePath = "C:\\Users\\" + username + "\\AppData\\Local\\Packages\\";
		File file = new File(packagePath);

		if (file.exists()) {
			for (String subfolder : file.list()) {
				if (subfolder.contains("SummonersWarOptimizer")) {
					String optimizerPath = packagePath + subfolder + "\\LocalState\\optimizerSaved.json";
					file = new File(optimizerPath);
					if (file.exists()) {
						useOptimizerIntegration = true;
						optimizerJsonFilePath = optimizerPath;
						System.out.println("Optimizer data found, integration enabled.");
					}
				}
			}
		}

		String importPath = SWAssistantProperties.getImportPath();
		Settings.readSettings(importPath);

		if (useOptimizerIntegration) {
			ImportFrame.importRunesFileFromOptimizer();
		} else {
			if (importPath != "") {
				ImportFrame.importRunesFile(filePaths.getRunesFilePath());
			}
		}

		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					displayFrame(RuneDisplayFrame.class);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	public static void pickRandomDungeon() {
		String dungeon = "";

		int i = random.nextInt(3);
		switch (i) {
		case 0:
			dungeon = "Giants";
			break;
		case 1:
			dungeon = "Dragons";
			break;
		default:
			dungeon = "Necropolis";
			break;
		}

		JOptionPane.showMessageDialog(currentFrame, dungeon, "Random dungeon", JOptionPane.PLAIN_MESSAGE);
	}

}
