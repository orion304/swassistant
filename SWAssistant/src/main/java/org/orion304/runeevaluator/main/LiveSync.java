package main.java.org.orion304.runeevaluator.main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Timer;

import org.json.JSONArray;
import org.json.JSONObject;

import main.java.org.orion304.runeevaluator.data.RawDataTranslator;
import main.java.org.orion304.runeevaluator.enums.StatType;
import main.java.org.orion304.runeevaluator.guildbattlelog.GuildBattleLog;
import main.java.org.orion304.runeevaluator.panels.WizardStatsPanel;

public class LiveSync implements ActionListener {

	private static boolean hasInitialized = false;

	private WizardStatsPanel wizardStatsPanel;

	Timer timer;
	int speed = 200;
	int delay = 0;

	int pauseTimer = 0;

	String path;
	File file;
	long lastModifiedTime;
	RandomAccessFile raf;
	long position;

	public LiveSync(WizardStatsPanel wizardPanel) {
		if (hasInitialized) {
			return;
		}

		String ob = "{\"item_list\": [{\"is_boxing\": 0, \"type\": 29, \"id\": 5003, \"quantity\": 6}, {\"is_boxing\": 0, \"type\": 29, \"id\": 6001, \"quantity\": 2}, {\"info\": {\"occupied_type\": 2, \"extra\": 3, \"sell_value\": 21556, \"pri_eff\": [4, 11], \"prefix_eff\": [10, 4], \"slot_no\": 6, \"rank\": 3, \"occupied_id\": 0, \"sec_eff\": [[3, 18], [11, 6]], \"wizard_id\": 8200451, \"upgrade_curr\": 0, \"rune_id\": 16068875112, \"base_value\": 431120, \"class\": 6, \"set_id\": 19, \"upgrade_limit\": 15}, \"is_boxing\": 1, \"type\": 8, \"id\": 19, \"quantity\": 1}], \"rift_dungeon_box_id\": 8, \"ret_code\": 0, \"tvaluelocal\": 1499216159, \"ts_val\": 1165106327, \"wizard_info\": {\"arena_energy_next_gain\": 818, \"wizard_level\": 50, \"darkportal_energy\": 6, \"unit_depository_slots\": {\"upgrade\": {\"mana\": 200000, \"number\": 210, \"crystal\": 50}, \"number\": 200}, \"energy_per_min\": 0.26, \"costume_point\": 105, \"social_point_max\": 3000, \"next_energy_gain\": 124, \"unit_slots\": {\"number\": 100}, \"wizard_energy\": 45, \"wizard_last_login\": \"2017-07-05 21:33:55\", \"mail_box_event\": 1, \"arena_energy\": 3, \"social_point_current\": 20, \"gain_exp\": 985, \"arena_energy_max\": 10, \"wizard_id\": 8200451, \"honor_point\": 477, \"wizard_name\": \"Orion304\", \"darkportal_energy_max\": 10, \"rep_assigned\": 1, \"rep_unit_id\": 7579080004, \"wizard_mana\": 3797122, \"pvp_event\": 0, \"energy_max\": 90, \"experience\": 3800000, \"honor_medal\": 44, \"wizard_crystal\": 5696, \"guild_point\": 2518, \"costume_point_max\": 9999}, \"best_clear_rift_dungeon_info\": {\"rift_dungeon_id\": 3001, \"rid\": 104571, \"best_rating\": 9, \"wizard_id\": 8200451, \"best_damage\": 2063292}, \"tzone\": \"America/Los_Angeles\", \"tvalue\": 1499273759, \"bestdeck_rift_dungeon_info\": {\"clear_rating\": 9, \"my_unit_list\": [[1, 7579080004, 14612, 6, 40], [2, 6236700620, 18313, 6, 40], [3, 5974432015, 12112, 6, 40], [4, 3299825761, 11014, 6, 40], [6, 3826318840, 15512, 6, 40], [7, 6542018511, 18713, 5, 35]], \"clear_damage\": 2063292, \"wizard_id\": 8200451, \"rift_dungeon_id\": 3001, \"leader_index\": 6}, \"command\": \"BattleRiftDungeonResult\", \"total_damage\": 1718895}";
		JSONObject job = new JSONObject(ob);
		Packet p = new Packet("type", ob, ob);
		this.processBattleRiftDungeonResult(p);
		/*
		 * String ob =
		 * "{\"tzone\": \"America/Los_Angeles\", \"wizard_info\": {\"arena_energy_next_gain\": 1064, \"wizard_level\": 50, \"darkportal_energy\": 6, \"unit_depository_slots\": {\"upgrade\": {\"mana\": 120000, \"number\": 160, \"crystal\": 30}, \"number\": 155}, \"energy_per_min\": 0.26, \"costume_point\": 166, \"social_point_max\": 3000, \"next_energy_gain\": 142, \"unit_slots\": {\"number\": 100}, \"wizard_energy\": 22, \"wizard_last_login\": \"2017-04-19 05:52:02\", \"mail_box_event\": 1, \"arena_energy\": 7, \"social_point_current\": 470, \"gain_exp\": 1200, \"arena_energy_max\": 10, \"wizard_id\": 8200451, \"honor_point\": 269, \"wizard_name\": \"Orion304\", \"darkportal_energy_max\": 10, \"rep_assigned\": 1, \"rep_unit_id\": 7579080004, \"wizard_mana\": 683238, \"pvp_event\": 1, \"energy_max\": 90, \"experience\": 3800000, \"honor_medal\": 75, \"wizard_crystal\": 5957, \"guild_point\": 3538, \"costume_point_max\": 9999}, \"battle_reward_list\": [{\"reward_list\": [{\"rune_set_id\": null, \"runecraft_type\": 2, \"rune_class\": null, \"runecraft_rank\": 4, \"rune_rank\": null, \"runecraft_effect_id\": 8, \"item_master_type\": 27, \"runecraft_set_id\": 4, \"runecraft_item_id\": 40804, \"item_quantity\": 1, \"unit_level\": null, \"item_master_id\": null, \"unit_class\": null, \"random_rune_type\": null}], \"wizard_id\": 7995968}, {\"reward_list\": [{\"rune_set_id\": null, \"runecraft_type\": 2, \"rune_class\": null, \"runecraft_rank\": 4, \"rune_rank\": null, \"runecraft_effect_id\": 8, \"item_master_type\": 27, \"runecraft_set_id\": 13, \"runecraft_item_id\": 130804, \"item_quantity\": 1, \"unit_level\": null, \"item_master_id\": null, \"unit_class\": null, \"random_rune_type\": null}], \"wizard_id\": 8200451}, {\"reward_list\": [], \"wizard_id\": 154}], \"ret_code\": 0, \"clear_time\": {\"is_new_record\": 0, \"current_time\": 338625, \"best_time\": 148785}, \"tvalue\": 1492566929, \"win_lose\": 1, \"tvaluelocal\": 1492509329, \"command\": \"BattleRiftOfWorldsRaidResult\", \"ts_val\": 1155122457, \"reward\": {\"crate\": {\"runecraft_info\": {\"sell_value\": 16000, \"craft_type_id\": 130804, \"craft_item_id\": 37941464, \"wizard_id\": 8200451, \"craft_type\": 2}}}}"
		 * ; JSONObject job = new JSONObject(ob); Packet p = new Packet("type",
		 * ob, ob); this.processBattleRiftOfWorldsRaidResultPacket(p);
		 */

		this.wizardStatsPanel = wizardPanel;

		path = Main.filePaths.getLogFilePath();

		file = new File(path);
		if (file.exists()) {
			output("Log file found, sync enabled");
			lastModifiedTime = file.lastModified();
			position = getFileLength(file);

			timer = new Timer(200, this);
			timer.setInitialDelay(delay);
			timer.start();
			hasInitialized = true;

		}

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			if (pauseTimer > 0) {
				pauseTimer--;
				return;
			}

			if (!file.exists()) {
				path = Main.filePaths.getLogFilePath();
				file = new File(path);
				if (file.exists()) {
					output("Log file found, sync enabled");
				} else {
					pauseTimer = 20;
				}
				return;
			}

			long modified = file.lastModified();
			if (lastModifiedTime != modified) {
				try {
					processChangedFile();
				} catch (Exception ex) {
					ex.printStackTrace();
				} finally {
					lastModifiedTime = modified;
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private long getFileLength(File file) {
		long length = -1;
		try {
			RandomAccessFile raf = new RandomAccessFile(file, "r");
			length = raf.length();
			raf.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return length;
	}

	private void output(Object object) {
		wizardStatsPanel.updateOutput(object.toString());
		System.out.println(object);
	}

	private boolean processBattleArenaStartPacket(Packet p) {
		JSONArray oppUnitList = p.jsonResponse.getJSONArray("opp_unit_list");
		for (int i = 0; i < oppUnitList.length(); i++) {
			JSONObject jsonUnit = oppUnitList.getJSONObject(i);
			Unit unit = new Unit(jsonUnit);
			output(unit.toString(i + 1));
		}
		return true;
	}

	private boolean processBattleDungeonResultPacket(Packet p) {
		if (!p.jsonResponse.has("reward")) {
			return false;
		}

		JSONObject reward = p.jsonResponse.getJSONObject("reward");

		if (!reward.has("crate")) {
			return false;
		}

		JSONObject crate = reward.getJSONObject("crate");

		if (!crate.has("rune")) {
			return false;
		}

		JSONObject jsonRune = crate.getJSONObject("rune");

		Rune rune = new Rune(jsonRune, true);
		Rune.reevaluateRunes();

		output("Rune drop:");
		output(rune.getEquippedMonsters(false));
		output(rune.getEquippedMonsters(true));
		output(rune);

		Rune.removeRune(rune);

		return true;
	}

	private boolean processBattleGuildWarStartPacket(Packet p) {
		JSONArray oppUnitListWave = p.jsonResponse.getJSONArray("guildwar_opp_unit_list");
		for (int i = 0; i < oppUnitListWave.length(); i++) {
			JSONArray oppUnitList = oppUnitListWave.getJSONArray(i);
			for (int j = 0; j < oppUnitList.length(); j++) {
				JSONObject jsonUnit = oppUnitList.getJSONObject(j);
				Unit unit = new Unit(jsonUnit);
				output(unit.toString(i * 3 + j + 1));
			}
		}
		return true;
	}

	private boolean processBattleRiftDungeonResult(Packet p) {
		if (!p.jsonResponse.has("item_list")) {
			return false;
		}

		JSONArray itemList = p.jsonResponse.getJSONArray("item_list");
		JSONObject item = itemList.getJSONObject(itemList.length() - 1);

		if (!item.has("info")) {
			return false;
		}

		JSONObject reward = item.getJSONObject("info");

		try {
			Rune rune = new Rune(reward, true);
			Rune.reevaluateRunes();

			output("Rune drop:");
			output(rune.getEquippedMonsters(false));
			output(rune.getEquippedMonsters(true));
			output(rune);

			Rune.removeRune(rune);

			return true;
		} catch (Exception e) {

		}

		try {
			RuneUpgradeItem upgradeItem = new RuneUpgradeItem(reward);
			String validUpgrades = upgradeItem.getValidRunes();

			if (validUpgrades == "") {
				return true;
			}

			output("Rune upgrade drop");
			output(validUpgrades);
			return true;
		} catch (Exception e) {

		}

		return false;
	}

	private boolean processBattleRiftOfWorldsRaidResultPacket(Packet p) {
		JSONObject response = p.jsonResponse;

		if (!response.has("reward")) {
			return false;
		}

		JSONObject reward = response.getJSONObject("reward");

		if (!reward.has("crate")) {
			return false;
		}

		JSONObject crate = reward.getJSONObject("crate");

		if (!crate.has("runecraft_info")) {
			return false;
		}

		try {
			JSONObject runecraftInfo = crate.getJSONObject("runecraft_info");
			RuneUpgradeItem item = new RuneUpgradeItem(runecraftInfo);
			String validUpgrades = item.getValidRunes();

			if (validUpgrades == "") {
				return true;
			}

			output("Rune upgrade drop");
			output(validUpgrades);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}

	private boolean processBuyShopItemPacket(Packet p) {
		if (!p.jsonResponse.has("reward")) {
			return false;
		}

		JSONObject reward = p.jsonResponse.getJSONObject("reward");

		if (!reward.has("crate")) {
			return false;
		}

		JSONObject crate = reward.getJSONObject("crate");

		if (!crate.has("runes")) {
			return false;
		}

		JSONArray jsonRunes = crate.getJSONArray("runes");
		for (int i = 0; i < jsonRunes.length(); i++) {

			JSONObject jsonRune = jsonRunes.getJSONObject(i);

			Rune rune = new Rune(jsonRune, true);
			Rune.reevaluateRunes();

			output("Rune drop:");
			output(rune.getEquippedMonsters(false));
			output(rune.getEquippedMonsters(true));
			output(rune);

			Rune.removeRune(rune);
		}

		return true;
	}

	private void processChangedFile() {
		try {
			RandomAccessFile raf = new RandomAccessFile(file, "r");
			raf.seek(position);
			position = raf.length();
			ArrayList<String> strings = new ArrayList<>();
			while (raf.getFilePointer() != position) {
				strings.add(raf.readLine());
			}
			raf.close();

			processPackets(strings);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean processGetGuildWarBattleLogByGuildId(Packet p) {
		int logType = p.jsonResponse.getInt("log_type");

		if (logType != 1) {
			return false;
		}

		JSONArray log = p.jsonResponse.getJSONArray("battle_log_list_group");

		for (int i = 0; i < log.length(); i++) {
			GuildBattleLog battleLog = new GuildBattleLog(log.getJSONObject(i));

			String fileName = battleLog.exportHeader + ".txt";
			Path file = Paths.get("C:\\Users\\Tokuro\\Google Drive\\Games\\Summoners War\\GW Logs\\" + fileName);

			String detailFileName = battleLog.exportHeader + " detailed.txt";
			Path detailFile = Paths
					.get("C:\\Users\\Tokuro\\Google Drive\\Games\\Summoners War\\GW Logs\\" + detailFileName);

			try {
				Files.write(file, battleLog.export, Charset.forName("UTF-8"));
				Files.write(detailFile, battleLog.exportLong, Charset.forName("UTF-8"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		return true;
	}

	private boolean processHubUserLogin(Packet p) {
		JSONObject response = p.jsonResponse;
		if (!response.has("guild")) {
			return false;
		}

		JSONObject guild = response.getJSONObject("guild");
		JSONArray members = guild.getJSONArray("guild_members");
		ArrayList<String> names = new ArrayList<>();
		String name;
		for (int i = 0; i < members.length(); i++) {
			name = members.getJSONObject(i).getString("wizard_name");
			names.add(name);
			output(name);
		}

		Path file = Paths.get("C:\\Users\\Tokuro\\Google Drive\\Games\\Summoners War\\members.txt");

		try {
			Files.write(file, names, Charset.forName("UTF-8"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;
	}

	private boolean processPacket(Packet p) {
		try {
			if (p.getPacketType().equalsIgnoreCase("UpgradeRune")) {
				return processUpgradeRunePacket(p);
				// } else if
				// (p.getPacketType().equalsIgnoreCase("BattleArenaStart")) {
				// return processBattleArenaStartPacket(p);
				// } else if
				// (p.getPacketType().equalsIgnoreCase("BattleGuildWarStart")) {
				// return processBattleGuildWarStartPacket(p);
			} else if (p.getPacketType().equalsIgnoreCase("BattleDungeonResult")) {
				return processBattleDungeonResultPacket(p);
			} else if (p.getPacketType().equalsIgnoreCase("BuyShopItem")) {
				return processBuyShopItemPacket(p);
			} else if (p.getPacketType().equalsIgnoreCase("BattleRiftOfWorldsRaidResult")) {
				return processBattleRiftOfWorldsRaidResultPacket(p);
				// } else if (p.getPacketType().equalsIgnoreCase("SellRune")) {
				// return processSellRunePacket(p);
				// } else if
				// (p.getPacketType().equalsIgnoreCase("GetGuildWarBattleLogByGuildId"))
				// {
				// return processGetGuildWarBattleLogByGuildId(p);
				// } else if
				// (p.getPacketType().equalsIgnoreCase("HubUserLogin")) {
				// return processHubUserLogin(p);
			} else if (p.getPacketType().equals("BattleRiftDungeonResult")) {
				return processBattleRiftDungeonResult(p);
			}
		} catch (

		Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	private void processPackets(List<String> strings) {

		String requestString = null;
		String responseString = null;
		String packetType = null;

		for (String string : strings) {
			if (string.startsWith("Request")) {
				packetType = string.substring(9, string.length() - 2);
				requestString = null;
				responseString = null;
				continue;
			}

			if (packetType != null && string.startsWith("{")) {
				if (requestString == null) {
					requestString = string;
				} else {
					responseString = string;
					Packet p = new Packet(packetType, requestString, responseString);
					wizardStatsPanel.updateStats(p);

					// output(p.getRequest());
					// output(p.getResponse());

					if (!processPacket(p)) {
						output(p.getPacketType());
					}
					packetType = null;
				}
				continue;
			}
		}

	}

	private boolean processSellRunePacket(Packet p) {
		output(p.toString());
		return true;
	}

	private boolean processUpgradeRunePacket(Packet p) {
		if (!p.jsonRequest.has("upgrade_curr")) {
			return false;
		}

		int previousLevel = p.jsonRequest.getInt("upgrade_curr");

		if (!p.jsonResponse.has("rune")) {
			return false;
		}

		JSONObject rune = p.jsonResponse.getJSONObject("rune");

		int currentLevel = rune.getInt("upgrade_curr");

		if (previousLevel == currentLevel) {
			output("Upgrade failed");
		} else {
			String append = "";
			if (currentLevel % 3 == 0) {
				JSONArray stats = rune.getJSONArray("sec_eff");

				int N = stats.length();

				for (int i = 0; i < N; i++) {
					JSONArray values = stats.getJSONArray(i);
					StatType type = RawDataTranslator.getStatType(values.getInt(0));
					int value = values.getInt(1);

					if (type == null) {
						append += "\n  Worthless " + value;
					} else {
						append += "\n  " + type.toString() + " " + value;
					}
				}
			}
			output("Upgrade successful " + append);
		}

		return true;
	}

}
