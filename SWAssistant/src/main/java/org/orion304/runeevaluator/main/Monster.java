package main.java.org.orion304.runeevaluator.main;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

import main.java.org.orion304.runeevaluator.enums.Dungeon;
import main.java.org.orion304.runeevaluator.enums.RuneType;
import main.java.org.orion304.runeevaluator.enums.RuneWorth;
import main.java.org.orion304.runeevaluator.enums.StatType;

public class Monster implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5773610474722131199L;
	private static long lastId = 1;

	public static ArrayList<Monster> monsters = new ArrayList<>();

	public static void clearMonsterRunes() {
		for (Monster monster : monsters) {
			monster.clearRunes();
		}
	}

	public static void clearMonsters() {
		monsters.clear();
	}

	public static boolean doesMonsterExist(String name) {
		for (Monster monster : monsters) {
			if (monster.name.equalsIgnoreCase(name)) {
				return true;
			}
		}

		return false;
	}

	public static String getRuneDistribution() {
		HashMap<Dungeon, Integer> runesPerDungeon = new HashMap<>();

		double totalNumberOfRunes = 0;
		for (Monster monster : monsters) {
			for (RuneType set : new RuneType[] { monster.set1, monster.set2, monster.set3 }) {
				if (set == null || set.dropsFrom == null) {
					continue;
				}

				totalNumberOfRunes += set.setSize;

				int runes = set.setSize;
				if (runesPerDungeon.containsKey(set.dropsFrom)) {
					runes += runesPerDungeon.get(set.dropsFrom);
				}

				runesPerDungeon.put(set.dropsFrom, runes);
			}
		}

		String text = "Runes (" + ((int) totalNumberOfRunes) + "):\n";
		for (Dungeon dungeon : runesPerDungeon.keySet()) {
			double runes = runesPerDungeon.get(dungeon);
			text += dungeon + ": " + ((int) runes) + "   (" + Math.round(runes / totalNumberOfRunes * 100) + "%)\n";
		}

		return text;
	}

	private final long id;
	private String name;

	private RuneType set1, set2, set3;
	private ArrayList<StatType> slot2 = new ArrayList<>();
	private ArrayList<StatType> slot4 = new ArrayList<>();

	private ArrayList<StatType> slot6 = new ArrayList<>();

	private HashMap<StatType, RuneWorth> worths = new HashMap<>();
	transient private ArrayList<Rune> runes;
	transient private HashMap<Integer, TreeMap<RuneRank, Rune>> runesBySlot;

	transient private HashMap<Integer, TreeMap<RuneRank, Rune>> runesBySlotOnlyInventory;

	public Monster() {
		id = lastId;
		lastId++;
		initializeRuneInfo();

		for (StatType statType : StatType.values()) {
			setWorth(statType, RuneWorth.NONE);
		}

		monsters.add(this);
	}

	public Monster(String name) {
		this();
		setName(name);
	}

	public Monster(String[] rawMonsterData) {
		this();
		setName(rawMonsterData[1]);

		int level = Integer.parseInt(rawMonsterData[2]);
		int grade = Integer.parseInt(rawMonsterData[3]);

		if (grade < 5 || (level == 1 && grade == 5)) {
			monsters.remove(this);
		}
	}

	public void addRune(Rune rune) {
		if (runes == null) {
			initializeRuneInfo();
		}
		runes.add(rune);
		RuneRank runeRank = new RuneRank(rune, this);
		runesBySlot.get(rune.slot).put(runeRank, rune);

	}

	public void addRuneOnlyInventory(Rune rune) {
		if (runes == null) {
			initializeRuneInfo();
		}

		if (!runes.contains(rune)) {
			runes.add(rune);
		}

		RuneRank runeRank = new RuneRank(rune, this);
		runesBySlotOnlyInventory.get(rune.slot).put(runeRank, rune);
	}

	public void clearRunes() {
		if (runes == null) {
			initializeRuneInfo();
		}
		runes.clear();
		for (int slot : runesBySlot.keySet()) {
			runesBySlot.get(slot).clear();
			runesBySlotOnlyInventory.get(slot).clear();
		}

	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Monster other = (Monster) obj;
		if (id != other.id) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	public String getMainStats() {
		String str = "";
		boolean atLeastOneStat = false;
		for (List<StatType> stats : Arrays.asList(slot2, slot4, slot6)) {
			atLeastOneStat = false;
			for (StatType stat : stats) {
				str += stat + ", ";
				atLeastOneStat = true;
			}

			if (atLeastOneStat) {
				str = str.substring(0, str.length() - 2);
			} else {
				str += "???";
			}

			str += "/";
		}

		str = str.substring(0, str.length() - 1);
		return str;
	}

	public String getName() {
		return name;
	}

	public int getRuneRank(Rune rune, boolean inventoryOnly) {
		int slot = rune.slot;

		// if (!runesBySlot.get(slot).values().contains(rune)) {
		// return 0;
		// }

		int i = 1;

		Set<RuneRank> ranks;

		if (inventoryOnly) {
			ranks = runesBySlotOnlyInventory.get(slot).keySet();
		} else {
			ranks = runesBySlot.get(slot).keySet();
		}

		for (RuneRank runeRank : ranks) {
			if (runeRank.rune.equals(rune)) {
				return i;
			}
			i++;
		}
		return 0;
	}

	public List<Rune> getRunesBySlot(int slot) {
		ArrayList<Rune> runes = new ArrayList<>();
		for (Rune rune : this.runes) {
			if (rune.slot == slot) {
				runes.add(rune);
			}
		}
		return runes;
	}

	public String getRuneSets() {
		String str = "";
		if (set1 != null) {
			str += set1.toString();
			if (set2 != null || set3 != null) {
				str += "/";
			}
		}

		if (set2 != null) {
			str += set2.toString();
			if (set3 != null) {
				str += "/";
			}
		}

		if (set3 != null) {
			str += set3.toString();
		}

		return str;
	}

	public RuneType getSet1() {
		return set1;
	}

	public RuneType getSet2() {
		return set2;
	}

	public RuneType getSet3() {
		return set3;
	}

	public ArrayList<StatType> getSlot2() {
		return slot2;
	}

	public ArrayList<StatType> getSlot4() {
		return slot4;
	}

	public ArrayList<StatType> getSlot6() {
		return slot6;
	}

	public TreeMap<RuneRank, Rune> getSortedRunesBySlot(int slot) {
		return runesBySlot.get(slot);
	}

	public String getStatsByWorth(RuneWorth runeWorth) {
		String str = "";
		for (StatType statType : StatType.values()) {
			if (getWorth(statType) == runeWorth) {
				str += statType + ", ";
			}
		}

		if (str != "") {
			str = str.substring(0, str.length() - 2);
		}

		return str;
	}

	public RuneWorth getWorth(StatType statType) {
		return worths.get(statType);
	}

	public boolean hasFlexibleSet() {
		return set1 == RuneType.ANY || set2 == RuneType.ANY || set3 == RuneType.ANY;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	private void initializeRuneInfo() {
		runes = new ArrayList<>();
		runesBySlot = new HashMap<>();
		runesBySlotOnlyInventory = new HashMap<>();
		for (int i = 1; i <= 6; i++) {
			runesBySlot.put(i, new TreeMap<>());
			runesBySlotOnlyInventory.put(i, new TreeMap<>());
		}
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSet1(RuneType set1) {
		this.set1 = set1;
	}

	public void setSet2(RuneType set2) {
		this.set2 = set2;
	}

	public void setSet3(RuneType set3) {
		this.set3 = set3;
	}

	public void setWorth(StatType statType, RuneWorth runeWorth) {
		this.worths.put(statType, runeWorth);
	}

	@Override
	public String toString() {
		return name;
		// return "Monster [name=" + name + ", runesBySlot=" + runesBySlot +
		// "]";
	}

}
