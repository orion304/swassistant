package main.java.org.orion304.runeevaluator.main;

import org.json.JSONObject;

import main.java.org.orion304.runeevaluator.enums.AllStatType;
import main.java.org.orion304.runeevaluator.enums.RuneType;
import main.java.org.orion304.runeevaluator.enums.RuneUpgradeType;
import main.java.org.orion304.runeevaluator.enums.StatType;

public class RuneUpgradeItem {

	private final RuneUpgradeType runeUpgradeType;
	private final AllStatType allStatType;
	private final RuneType runeType;
	private final StatType statType;
	private final int grade;

	public RuneUpgradeItem(JSONObject item) {

		String data = Integer.toString(item.getInt("craft_type_id"));

		int type = item.getInt("craft_type");
		int set = Integer.parseInt(data.substring(0, data.length() - 4));
		int stat = Integer.parseInt(data.substring(data.length() - 4, data.length() - 2));

		runeUpgradeType = RuneUpgradeType.getById(type);
		runeType = RuneType.getById(set);

		allStatType = AllStatType.getById(stat);
		statType = allStatType == null ? null : allStatType.statType;

		grade = Integer.parseInt(data.substring(data.length() - 1));

	}

	private boolean canEnchantRune(Rune rune) {
		if (this.statType == null) {
			return false;
		}

		if (rune.slot == 1 && this.statType == StatType.DEF) {
			return false;
		}

		if (rune.slot == 3 && this.statType == StatType.ATK) {
			return false;
		}

		for (StatType statType : rune.statTypes) {
			if (this.statType.equals(statType)) {
				return false;
			}
		}

		return true;

	}

	private boolean canGrindRune(Rune rune) {
		if (this.statType == null) {
			return false;
		}

		StatType runeStatType;
		AllStatType allStatType;

		if (rune.allStatTypes.size() > 0) {

			for (int i = 0; i < rune.allStatTypes.size(); i++) {
				if (i <= 1) {
					continue;
				}

				allStatType = rune.allStatTypes.get(i);

				if (this.allStatType.equals(allStatType)) {
					return true;
				}
			}

		} else {
			for (int i = 0; i < rune.statTypes.size(); i++) {
				if (i <= 1) {
					continue;
				}

				runeStatType = rune.statTypes.get(i);

				if (this.statType.equals(runeStatType)) {
					return true;
				}
			}
		}

		return false;
	}

	private boolean canUpgradeRune(Rune rune) {
		if (rune.runeType != this.runeType) {
			return false;
		}

		if (runeUpgradeType == RuneUpgradeType.ENCHANTMENT) {
			return canEnchantRune(rune);
		} else {
			return canGrindRune(rune);
		}
	}

	public String getValidRunes() {
		if (statType == null) {
			return "";
		}

		StringBuilder b = new StringBuilder();
		String eol = System.lineSeparator();

		for (Rune rune : Rune.runes) {
			if (canUpgradeRune(rune)) {
				b.append(rune.equippedTo);
				b.append(":\t(");
				b.append(rune.slot);
				b.append(") ");
				for (String effect : rune.effects) {
					b.append(effect);
					b.append(", ");
				}
				b.deleteCharAt(b.length() - 1);
				b.deleteCharAt(b.length() - 1);
				b.append(eol);
			}
		}

		return b.toString();
	}

}
