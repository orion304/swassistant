package main.java.org.orion304.runeevaluator.main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class SWAssistantProperties {

	private static boolean isLoaded = false;
	private static final Properties properties = new Properties();
	private static final String propertiesFilename = "runeevaluator.properties";

	public static final double multiplier = 100;

	public static String getImportPath() {
		loadIfNotLoaded();
		return properties.getProperty("ImportFilepath", "");
	}

	private static void loadIfNotLoaded() {
		if (!isLoaded) {
			loadProperties();
		}
	}

	public static void loadProperties() {
		InputStream input = null;
		File f = new File(propertiesFilename);

		System.out.println(f.getAbsolutePath());
		if (!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		try {
			input = new FileInputStream(f);
			properties.load(input);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		isLoaded = true;
	}

	public static void saveProperties() {
		OutputStream output = null;
		File f = new File(propertiesFilename);

		try {
			output = new FileOutputStream(f);
			properties.store(output, "Rune Evaluator Properties");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void setImportPath(String filePath) {
		loadIfNotLoaded();
		properties.setProperty("ImportFilepath", filePath);
	}

}
