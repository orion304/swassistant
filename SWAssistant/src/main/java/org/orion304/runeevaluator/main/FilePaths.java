package main.java.org.orion304.runeevaluator.main;

import java.io.Serializable;

public class FilePaths implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 9140300219410908149L;
	private String runesFilePath = "";
	private String winRunesFilePath = "";
	private String monstersFilePath = "";
	private String winMonstersFilePath = "";
	private String settingsFilePath = "";
	private String winSettingsFilePath = "";
	private String logFilePath = "";
	private String winLogFilePath = "";

	public FilePaths() {

	}

	public String getLogFilePath() {
		if (logFilePath == null) {
			logFilePath = "";
		}

		if (winLogFilePath == null) {
			winLogFilePath = "";
		}
		return Main.isWindows ? winLogFilePath : logFilePath;
	}

	public String getMonstersFilePath() {
		if (monstersFilePath == null) {
			monstersFilePath = "";
		}

		if (winMonstersFilePath == null) {
			winMonstersFilePath = "";
		}

		return Main.isWindows ? winMonstersFilePath : monstersFilePath;
	}

	public String getRunesFilePath() {
		if (runesFilePath == null) {
			runesFilePath = "";
		}

		if (winRunesFilePath == null) {
			winRunesFilePath = "";
		}

		return Main.isWindows ? winRunesFilePath : runesFilePath;
	}

	public String getSettingsFilePath() {
		if (settingsFilePath == null) {
			settingsFilePath = "";
		}

		if (winSettingsFilePath == null) {
			winSettingsFilePath = "";
		}

		return Main.isWindows ? winSettingsFilePath : settingsFilePath;
	}

	public void setLogFilePath(String logFilePath) {
		if (Main.isWindows) {
			this.winLogFilePath = logFilePath;
		} else {
			this.logFilePath = logFilePath;
		}
	}

	public void setMonstersFilePath(String monstersFilePath) {
		if (Main.isWindows) {
			this.winMonstersFilePath = monstersFilePath;
		} else {
			this.monstersFilePath = monstersFilePath;
		}
	}

	public void setRunesFilePath(String runesFilePath) {
		if (Main.isWindows) {
			this.winRunesFilePath = runesFilePath;
		} else {
			this.runesFilePath = runesFilePath;
		}
	}

	public void setSettingsFilePath(String settingsFilePath) {
		if (Main.isWindows) {
			this.winSettingsFilePath = settingsFilePath;
		} else {
			this.settingsFilePath = settingsFilePath;
		}
	}

}
