package main.java.org.orion304.runeevaluator.main;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import main.java.org.orion304.runeevaluator.enums.AllStatType;
import main.java.org.orion304.runeevaluator.enums.RuneType;

public class Unit {

	private int attribute;
	private int resist;
	private int level;
	private int spd;
	private final int baseSpd;
	private int critRate;
	private String runeBreakdown;
	private int accuracy;
	private int atk;
	private int def;
	private int con;
	private int hp;
	private int critDmg;

	private int atkF, defF, hpF;
	private int atkP, defP, hpP;

	private ArrayList<JSONObject> runes = new ArrayList<>();
	private HashMap<RuneType, Integer> runeTypes = new HashMap<>();
	private String runeSets = "";

	public Unit(JSONObject jsonUnit) {
		JSONObject jsonUnitInfo = jsonUnit.getJSONObject("unit_info");
		attribute = jsonUnitInfo.getInt("attribute");
		resist = jsonUnitInfo.getInt("resist");
		level = jsonUnitInfo.getInt("unit_level");
		spd = jsonUnitInfo.getInt("spd");
		baseSpd = spd;
		critRate = jsonUnitInfo.getInt("critical_rate");
		Object runesObject = jsonUnitInfo.get("runes");

		if (runesObject instanceof JSONObject) {
			JSONObject jsonRunes = (JSONObject) runesObject;
			JSONObject rune;
			for (String runeKey : jsonRunes.keySet()) {
				rune = jsonRunes.getJSONObject(runeKey);
				runes.add(rune);
			}
		} else if (runesObject instanceof JSONArray) {
			JSONArray jsonRunes = jsonUnitInfo.getJSONArray("runes");
			JSONObject rune;
			for (int i = 0; i < jsonRunes.length(); i++) {
				rune = jsonRunes.getJSONObject(i);
				runes.add(rune);
			}
		}

		accuracy = jsonUnitInfo.getInt("accuracy");
		atk = jsonUnitInfo.getInt("atk");
		def = jsonUnitInfo.getInt("def");
		con = jsonUnitInfo.getInt("con");
		hp = con * 15;
		critDmg = jsonUnitInfo.getInt("critical_damage");

		for (JSONObject x : runes) {
			handleRune(x);
		}

		getRuneSets();
	}

	private void addRuneEffect(JSONArray effect) {
		int effectType = effect.getInt(0);
		int effectValue = effect.getInt(1);

		AllStatType type = AllStatType.getById(effectType);

		if (type == null) {
			return;
		}

		switch (type) {
		case HP_flat:
			hpF += effectValue;
			break;
		case HP:
			hpP += effectValue;
			break;
		case ATK_flat:
			atkF += effectValue;
			break;
		case ATK:
			atkP += effectValue;
			break;
		case DEF_flat:
			defF += effectValue;
			break;
		case DEF:
			defP += effectValue;
			break;
		case SPD:
			spd += effectValue;
			break;
		case CRate:
			critRate += effectValue;
			break;
		case CDmg:
			critDmg += effectValue;
			break;
		case RES:
			resist += effectValue;
			break;
		case ACC:
			accuracy += effectValue;
			break;
		}
	}

	public int getAccuracy() {
		return accuracy;
	}

	public int getAtk() {
		return atk + atkF + (int) (atk * (atkP / 100.));
	}

	public int getAttribute() {
		return attribute;
	}

	public int getBaseSpd() {
		return baseSpd;
	}

	public int getCon() {
		return con;
	}

	public int getCritDmg() {
		return critDmg;
	}

	public int getCritRate() {
		return critRate;
	}

	public int getDef() {
		return def + defF + (int) (def * (defP / 100.));
	}

	public int getHp() {
		return hp + hpF + (int) (hp * (hpP / 100.));
	}

	public int getLevel() {
		return level;
	}

	public int getResist() {
		return resist;
	}

	public String getRuneBreakdown() {
		return runeBreakdown;
	}

	private void getRuneSets() {
		int n;
		for (RuneType runeType : runeTypes.keySet()) {
			n = runeTypes.get(runeType);
			if (n >= runeType.setSize) {
				runeSets += runeType.toString() + " / ";
			}
		}

		if (runeSets.length() > 0) {
			runeSets = runeSets.substring(0, runeSets.length() - 3);
		}
	}

	public int getSpd() {
		return spd;
	}

	private void handleRune(JSONObject rune) {
		int set = rune.getInt("set_id");
		RuneType runeType = RuneType.getById(set);

		if (runeTypes.containsKey(runeType)) {
			runeTypes.put(runeType, runeTypes.get(runeType) + 1);
		} else {
			runeTypes.put(runeType, 1);
		}

		addRuneEffect(rune.getJSONArray("pri_eff"));
		addRuneEffect(rune.getJSONArray("prefix_eff"));
		JSONArray secEffects = rune.getJSONArray("sec_eff");
		for (int i = 0; i < secEffects.length(); i++) {
			addRuneEffect(secEffects.getJSONArray(i));
		}
	}

	public String toString(int i) {
		String string = "";
		string += "Unit " + i + "\t\t";
		string += "Level " + level + "\n";
		string += runeSets + "\n";
		string += "  SPD " + spd + "\t\t";
		string += "ACC " + accuracy + "\n";
		string += "  HP " + getHp() + "\t\t";
		string += "ATK " + getAtk() + "\n";
		string += "  DEF " + getDef() + "\t\t";
		string += "CR " + critRate + "\n";
		string += "  RES " + resist + "\t\t";
		string += "CD " + critDmg + "\n";
		return string;
	}

}
