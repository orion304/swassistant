package main.java.org.orion304.runeevaluator.main;

public class RuneRank implements Comparable<RuneRank> {

	public Rune rune;
	public Monster monster;
	public double value;

	public RuneRank(Rune rune, Monster monster) {
		this.rune = rune;
		this.monster = monster;
		value = rune.upgRuneWorth.get(monster);
	}

	@Override
	public int compareTo(RuneRank runeRank) {
		int result;
		result = -Double.compare(this.value, runeRank.value);
		if (result != 0) {
			return result;
		}

		result = -monster.getName().compareTo(runeRank.monster.getName());

		if (result != 0) {
			return result;
		}

		return -Integer.compare(this.rune.runeId, runeRank.rune.runeId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RuneRank other = (RuneRank) obj;
		if (monster == null) {
			if (other.monster != null) {
				return false;
			}
		} else if (!monster.equals(other.monster)) {
			return false;
		}
		if (rune == null) {
			if (other.rune != null) {
				return false;
			}
		} else if (!rune.equals(other.rune)) {
			return false;
		}
		if (Double.doubleToLongBits(value) != Double.doubleToLongBits(other.value)) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((monster == null) ? 0 : monster.hashCode());
		result = prime * result + ((rune == null) ? 0 : rune.hashCode());
		long temp;
		temp = Double.doubleToLongBits(value);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public String toString() {
		return "RuneRank [rune=" + rune.runeId + ", monster=" + monster.getName() + ", value=" + value + "]";
	}

}
