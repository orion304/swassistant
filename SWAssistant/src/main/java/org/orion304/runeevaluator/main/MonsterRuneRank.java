package main.java.org.orion304.runeevaluator.main;

public class MonsterRuneRank implements Comparable<MonsterRuneRank> {

	public Monster monster;
	public int place;
	public double value;

	public MonsterRuneRank(Monster monster, int place, double value) {
		this.monster = monster;
		this.place = place;
		this.value = value;
	}

	@Override
	public int compareTo(MonsterRuneRank arg0) {
		int result;
		result = Integer.compare(this.place, arg0.place);
		if (result != 0) {
			return result;
		}

		result = Double.compare(arg0.value, this.value);
		if (result != 0) {
			return result;
		}

		return this.monster.getName().compareTo(arg0.monster.getName());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		MonsterRuneRank other = (MonsterRuneRank) obj;
		if (monster == null) {
			if (other.monster != null) {
				return false;
			}
		} else if (!monster.equals(other.monster)) {
			return false;
		}
		if (place != other.place) {
			return false;
		}
		if (Double.doubleToLongBits(value) != Double.doubleToLongBits(other.value)) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((monster == null) ? 0 : monster.hashCode());
		result = prime * result + place;
		long temp;
		temp = Double.doubleToLongBits(value);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public String toString() {
		String str = "";
		str += monster.getName();
		str += " (" + place + ", ";
		str += DoubleRenderer.df.format(value).replaceAll(",", ".") + ")";
		return str;
	}

}
